package com.startdust.lubogo.activity.registry;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.home.MainActivity;
import com.startdust.lubogo.model.User;

public class RegistryActivity extends AppCompatActivity implements RegistryMVP.ViewMVP {
    EditText etName, etApellidoMaterno, etapellidoPaterno, etPassword, etEmail;
    Button btnRegistryUser;

    RegistryPresenter registryPresenter;

    @Override
    public void onCreate(Bundle savedIntanceState){
        super.onCreate(savedIntanceState);
        setContentView(R.layout.activity_registry);

        etName = findViewById(R.id.etName);
        etApellidoMaterno = findViewById(R.id.etApellidoMaterno);
        etapellidoPaterno = findViewById(R.id.etApellidoPaterno);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnRegistryUser = findViewById(R.id.btnRegistryUser);

        registryPresenter = new RegistryPresenter(this, this);

        btnRegistryUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User();
                user.setNombre(etName.getText().toString());
                user.setApellidoMaterno(etApellidoMaterno.getText().toString());
                user.setApellidoPaterno(etapellidoPaterno.getText().toString());
                user.setEmail(etEmail.getText().toString());
                user.setPassword(etPassword.getText().toString());
                registryPresenter.registry(user);
            }
        });
    }

    @Override
    public void onSuccessRegistry() {
        Toast.makeText(this, "Registro Exitoso", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(RegistryActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onError(String error) {

    }
}
