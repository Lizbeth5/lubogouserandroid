package com.startdust.lubogo.retrofit.model;

public class GetListProductsSavedData {
    String idProducto;
    String producto;
    String descripcion;
    String precio;
    String urlFoto;
    String idEstablecimiento;
    String nombre;
    String idTipoEstablecimiento;
    String Descripcion;

    public GetListProductsSavedData() {
    }

    public GetListProductsSavedData(String idProducto, String producto, String descripcion, String precio, String urlFoto, String idEstablecimiento, String nombre, String idTipoEstablecimiento, String descripcion1) {
        this.idProducto = idProducto;
        this.producto = producto;
        this.descripcion = descripcion;
        this.precio = precio;
        this.urlFoto = urlFoto;
        this.idEstablecimiento = idEstablecimiento;
        this.nombre = nombre;
        this.idTipoEstablecimiento = idTipoEstablecimiento;
        Descripcion = descripcion1;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public void setIdEstablecimiento(String idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdTipoEstablecimiento() {
        return idTipoEstablecimiento;
    }

    public void setIdTipoEstablecimiento(String idTipoEstablecimiento) {
        this.idTipoEstablecimiento = idTipoEstablecimiento;
    }
}
