package com.startdust.lubogo.activity.cuztomizerOrder;

import android.app.Activity;

import com.startdust.lubogo.retrofit.model.GetInformationProductEstablishmentResult;
import com.startdust.lubogo.retrofit.model.GetListDetailsProductsResult;

import java.util.ArrayList;

public class CustomizerOrderPresenter implements CustomizerOrderMVP.PresenterMVP {
    CustomizerOrderInteractor customizerOrderInteractor;
    CustomizerOrderMVP.ViewMVP viewMVP;
    Activity activity;

    public CustomizerOrderPresenter(CustomizerOrderMVP.ViewMVP viewMVP, Activity activity) {
        this.viewMVP = viewMVP;
        this.activity = activity;

        customizerOrderInteractor = new CustomizerOrderInteractor(activity,this);

    }

    @Override
    public void onSuccessInformationProduct(GetInformationProductEstablishmentResult getInformationProductEstablishmentResult) {
        viewMVP.onSuccessInformationProduct(getInformationProductEstablishmentResult);
    }

    @Override
    public void onSuccesListDetailsProduct(ArrayList<GetListDetailsProductsResult> getListDetailsProductsResults) {
        viewMVP.onSuccesListDetailsProduct(getListDetailsProductsResults);
    }

    @Override
    public void onError(String error) {
        viewMVP.onError(error);
    }

    public void informationProduct(String id){
        customizerOrderInteractor.informationProduct(id);
    }

    public void listDetailsProducts(String id){
        customizerOrderInteractor.listDetailsProduct(id);
    }
}
