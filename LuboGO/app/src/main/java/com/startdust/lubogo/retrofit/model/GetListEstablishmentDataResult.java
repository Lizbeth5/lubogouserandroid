package com.startdust.lubogo.retrofit.model;

import java.util.ArrayList;

public class GetListEstablishmentDataResult {
    ArrayList<GetListEstablishmentData> Data;

    public GetListEstablishmentDataResult() {
    }

    public GetListEstablishmentDataResult(ArrayList<GetListEstablishmentData> data) {
        Data = data;
    }

    public ArrayList<GetListEstablishmentData> getData() {
        return Data;
    }

    public void setData(ArrayList<GetListEstablishmentData> data) {
        Data = data;
    }
}
