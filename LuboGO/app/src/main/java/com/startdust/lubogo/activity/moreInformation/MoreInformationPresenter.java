package com.startdust.lubogo.activity.moreInformation;

import android.app.Activity;

import com.startdust.lubogo.retrofit.model.GetInformationProfileEstablishmentResult;
import com.startdust.lubogo.retrofit.model.GetListScheduleServiceEstablishmentData;

import java.util.ArrayList;

public class MoreInformationPresenter implements MoreInformationMVP.PresenterMVP {
    Activity activity;
    MoreInformationMVP.ViewMVP viewMVP;
    MoreInformationInteractor interactor;

    public MoreInformationPresenter(Activity activity, MoreInformationMVP.ViewMVP viewMVP) {
        this.activity = activity;
        this.viewMVP = viewMVP;

        interactor = new MoreInformationInteractor(activity,this);
    }

    @Override
    public void onSuccessInformationProfile(GetInformationProfileEstablishmentResult getInformationProfileEstablishmentResult) {
        viewMVP.onSuccessInformationProfile(getInformationProfileEstablishmentResult);
    }

    @Override
    public void onSuccessListScheduleServiceEstablishment(ArrayList<GetListScheduleServiceEstablishmentData> getListScheduleServiceEstablishmentData) {
        viewMVP.onSuccessListScheduleServiceEstablishment(getListScheduleServiceEstablishmentData);
    }

    @Override
    public void onError(String error) {
        viewMVP.onError(error);
    }

    public void informationEstablishment(String id){
        interactor.informationEstablishment(id);
    }

    public void listScheduleServiceStablishment(String id){
        interactor.listScheduleServiceEstablishment(id);
    }
}
