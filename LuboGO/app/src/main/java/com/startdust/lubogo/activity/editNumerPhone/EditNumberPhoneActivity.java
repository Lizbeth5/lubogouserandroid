package com.startdust.lubogo.activity.editNumerPhone;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;

public class EditNumberPhoneActivity extends AppCompatActivity {
    ImageView ivReturnInformationUser;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_number_phone);

        ivReturnInformationUser = findViewById(R.id.ivReturnInformationUser1);

        ivReturnInformationUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
