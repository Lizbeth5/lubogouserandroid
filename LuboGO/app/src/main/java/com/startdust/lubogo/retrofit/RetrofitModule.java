package com.startdust.lubogo.retrofit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitModule {
    RetrofitAPI.SetRegistry setRegistry;
    RetrofitAPI.PostLogin postLogin;
    RetrofitAPI.GetData getData;
    RetrofitAPI.GetInformationUser getInformationUser;
    RetrofitAPI.PutUpdateInformation putUpdateInformation;
    RetrofitAPI.GetListEstablishment getListEstablishment;
    RetrofitAPI.GetInformationProfileEstablishment getInformationProfileEstablishment;
    RetrofitAPI.GetListOfferts getListOfferts;
    RetrofitAPI.GetListCategories getListCategories;
    RetrofitAPI.GetListProductsEstablishment getListProductsEstablishment;
    RetrofitAPI.GetInformationProduct getInformationProduct;
    RetrofitAPI.GetListScheduleServiceEstablishment getListScheduleServiceEstablishment;
    RetrofitAPI.GetListProductsSaved getListProductsSaved;
    RetrofitAPI.GetListDetailsProducts getListDetailsProducts;
    RetrofitAPI.PostSavedProduct postSavedProduct;
    RetrofitAPI.PutDeleteProductSaved putDeleteProductSaved;

    private final String ROOT = "http://go.lubo.com.mx/back-lubogo/public/";

    //para ver los errores
    public RetrofitAPI.SetRegistry setRegistry(){
        if(setRegistry == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            setRegistry = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.SetRegistry.class);
        }
        return setRegistry;
    }

    public RetrofitAPI.PostLogin postLogin(){
        if(postLogin == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            postLogin = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.PostLogin.class);
        }
        return postLogin;
    }

    public RetrofitAPI.GetData getData(){
        if(getData == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            getData = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.GetData.class);
        }
        return getData;
    }

    public RetrofitAPI.GetInformationUser getInformationUser(){
        if(getInformationUser == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            getInformationUser = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.GetInformationUser.class);
        }
        return getInformationUser;
    }

    public RetrofitAPI.PutUpdateInformation putUpdateInformation(){
        if(putUpdateInformation == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            putUpdateInformation = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.PutUpdateInformation.class);
        }
        return putUpdateInformation;
    }

    public RetrofitAPI.GetListEstablishment getListEstablishment(){
        if(putUpdateInformation == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            getListEstablishment = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.GetListEstablishment.class);
        }
        return getListEstablishment;
    }

    public RetrofitAPI.GetInformationProfileEstablishment getInformationProfileEstablishment(){
        if(getInformationProfileEstablishment == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            getInformationProfileEstablishment = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.GetInformationProfileEstablishment.class);
        }
        return getInformationProfileEstablishment;
    }

    public RetrofitAPI.GetListOfferts getListOfferts(){
        if(getListOfferts == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            getListOfferts = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.GetListOfferts.class);
        }
        return getListOfferts;
    }

    public RetrofitAPI.GetListCategories getListCategories(){
        if(getListCategories == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            getListCategories = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.GetListCategories.class);
        }
        return getListCategories;
    }

    public RetrofitAPI.GetListProductsEstablishment getListProductsEstablishment(){
        if(getListProductsEstablishment == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            getListProductsEstablishment = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.GetListProductsEstablishment.class);
        }
        return getListProductsEstablishment;
    }

    public RetrofitAPI.GetInformationProduct getInformationProduct(){
        if(getInformationProduct == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            getInformationProduct = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.GetInformationProduct.class);
        }
        return getInformationProduct;
    }

    public RetrofitAPI.GetListScheduleServiceEstablishment getListScheduleServiceEstablishment(){
        if(getListScheduleServiceEstablishment == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            getListScheduleServiceEstablishment = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.GetListScheduleServiceEstablishment.class);
        }
        return getListScheduleServiceEstablishment;
    }

    public RetrofitAPI.GetListProductsSaved getListProductsSaved(){
        if(getListProductsSaved == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            getListProductsSaved = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.GetListProductsSaved.class);
        }
        return getListProductsSaved;
    }

    public RetrofitAPI.GetListDetailsProducts getListDetailsProducts(){
        if(getListDetailsProducts == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            getListDetailsProducts = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.GetListDetailsProducts.class);
        }
        return getListDetailsProducts;
    }

    public RetrofitAPI.PostSavedProduct postSavedProduct(){
        if(postSavedProduct == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            postSavedProduct = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.PostSavedProduct.class);
        }
        return postSavedProduct;
    }

    public RetrofitAPI.PutDeleteProductSaved putDeleteProductSaved(){
        if(putDeleteProductSaved == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            putDeleteProductSaved = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(RetrofitAPI.PutDeleteProductSaved.class);
        }
        return putDeleteProductSaved;
    }

}


