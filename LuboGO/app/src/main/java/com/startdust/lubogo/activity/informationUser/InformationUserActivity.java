package com.startdust.lubogo.activity.informationUser;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.squareup.picasso.Picasso;
import com.startdust.lubogo.BuildConfig;
import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.editNumerPhone.EditNumberPhoneActivity;
import com.startdust.lubogo.activity.email.EditEmailActivity;
import com.startdust.lubogo.preferences.AppPreferences;
import com.startdust.lubogo.retrofit.model.GetInformationUserResult;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.UploadStatusDelegate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class InformationUserActivity extends AppCompatActivity implements InformationUserMVP.ViewMVP{
    ImageView ivReturnProfile, ivEditEmail, ivEditPhone, ivAddPhoto;
    CardView cvAdPhoto;
    LinearLayout ll1Inf, ll2Inf, ll3Inf, ll4Inf, ll5Inf, ll6Inf, ll7Inf;
    TextView tvCancelAddPhoto, tvEmailUser, tvNumberPhoneUser;
    Button btnTakePhoto, btnSelectGalery, btnSaveInformation;
    EditText etName, etLastName;
    CircleImageView civToShowPhotoUser;
    InformationUserPresenter informationUserPresenter;

    AppPreferences prefs;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private ProgressDialog progressDialog;

    private final String CARPETA_RAIZ = "misImagenes/";
    private final String RUTA_IMAGEN = CARPETA_RAIZ + "misFotos";
    final int COD_SELECCIONADA = 10;
    final int COD_FOTO = 20;
    private final String fileFormat = ".jpg";
    private final String filename = "photo";

    File dirUpload = null;
    File fileUpload = null;

    private final int CODECAMERA = 111;
    private final int CODEGALLERY = 222;

    @Override
    public void onCreate(Bundle savedIntanceState){
        super.onCreate(savedIntanceState);
        setContentView(R.layout.activity_information_user);

        ivReturnProfile = findViewById(R.id.ivReturnProfile);
        ivEditEmail = findViewById(R.id.ivEditEmail);
        ivEditPhone = findViewById(R.id.ivEditPhone);
        ivAddPhoto = findViewById(R.id.ivAddPhoto);
        cvAdPhoto = findViewById(R.id.cvAdPhoto);
        ll1Inf = findViewById(R.id.ll1Inf);
        ll2Inf = findViewById(R.id.ll2Inf);
        ll3Inf = findViewById(R.id.ll3Inf);
        ll4Inf = findViewById(R.id.ll4Inf);
        ll5Inf = findViewById(R.id.ll5Inf);
        ll6Inf = findViewById(R.id.ll6Inf);
        ll7Inf = findViewById(R.id.ll7Inf);
        tvCancelAddPhoto = findViewById(R.id.tvCancelAddPhoto);
        btnSelectGalery = findViewById(R.id.btnSelectGalery);
        btnTakePhoto = findViewById(R.id.btnTakePhoto);
        btnSaveInformation = findViewById(R.id.btnSaveInformation);
        etName = findViewById(R.id.etNameUser);
        etLastName = findViewById(R.id.etlastName);
        tvEmailUser = findViewById(R.id.tvEmailUser);
        tvNumberPhoneUser = findViewById(R.id.tvNumberPhoneUser);
        civToShowPhotoUser = findViewById(R.id.civToShowPhotoUser);

        prefs = new AppPreferences(this);
        informationUserPresenter = new InformationUserPresenter(this,this);
        progressDialog = new ProgressDialog(this);

        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
        // Or, you can define it manually.
        UploadService.NAMESPACE = "com.stardust.lubogo";


        ivReturnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivEditEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InformationUserActivity.this, EditEmailActivity.class);
                startActivity(intent);
            }
        });

        ivEditPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InformationUserActivity.this, EditNumberPhoneActivity.class);
                startActivity(intent);
            }
        });

        ivAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll1Inf.setBackgroundResource(R.color.colorBackgroundInformation);
                ll2Inf.setBackgroundResource(R.color.colorBackgroundInformation);
                ll2Inf.setAlpha(0.5f);
                ll3Inf.setBackgroundResource(R.color.colorBackgroundInformation);
                ll3Inf.setAlpha(0.5f);
                ll4Inf.setBackgroundResource(R.color.colorBackgroundInformation);
                ll4Inf.setAlpha(0.5f);
                ll5Inf.setBackgroundResource(R.color.colorBackgroundInformation);
                ll5Inf.setAlpha(0.5f);
                ll6Inf.setBackgroundResource(R.color.colorBackgroundInformation);
                ll6Inf.setAlpha(0.52f);
                ll7Inf.setBackgroundResource(R.color.colorBackgroundInformation);
                ll7Inf.setAlpha(0.5f);
                cvAdPhoto.setVisibility(View.VISIBLE);
            }
        });

        tvCancelAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll1Inf.setBackgroundResource(R.color.colorWhite);
                ll2Inf.setBackgroundResource(R.color.colorLinearLayout2);
                ll3Inf.setBackgroundResource(R.color.colorLinearLayout2);
                ll4Inf.setBackgroundResource(R.color.colorLinearLayout2);
                ll5Inf.setBackgroundResource(R.color.colorLinearLayout2);
                ll6Inf.setBackgroundResource(R.color.colorLinearLayout2);
                ll7Inf.setBackgroundResource(R.color.colorLinearLayout2);
                cvAdPhoto.setVisibility(View.GONE);
            }
        });

        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camara();
            }
        });

        btnSelectGalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galeria();
            }
        });

        etName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSaveInformation.setVisibility(View.VISIBLE);
            }
        });

        etLastName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSaveInformation.setVisibility(View.VISIBLE);
            }
        });

        btnSaveInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                informationUserPresenter.updateInformation(etName.getText().toString(), etLastName.getText().toString());
            }
        });

        informationUserPresenter.informationUser();

    }

    @Override
    public void onSuccessInformationUser(GetInformationUserResult getInformationUserResult) {
        etName.setText(getInformationUserResult.getNombre());
        etLastName.setText(getInformationUserResult.getApellidoPaterno());
        tvEmailUser.setText(getInformationUserResult.getEmail());
        tvNumberPhoneUser.setText(getInformationUserResult.getTelefono());

        if(getInformationUserResult.getUrlFoto() != null){
            Picasso.with(this)
                    .load(getInformationUserResult.getUrlFoto())
                    .into(civToShowPhotoUser);
        }else{
            civToShowPhotoUser.setImageResource(R.drawable.ic_photo_user);
        }

    }

    @Override
    public void onSuccessUpdateInformation() {
        Toast.makeText(this, "Actualización correcta", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onError(String error) {

    }

   public void removeMessageAddPhoto(){
       ll1Inf.setBackgroundResource(R.color.colorWhite);
       ll2Inf.setBackgroundResource(R.color.colorLinearLayout2);
       ll3Inf.setBackgroundResource(R.color.colorLinearLayout2);
       ll4Inf.setBackgroundResource(R.color.colorLinearLayout2);
       ll5Inf.setBackgroundResource(R.color.colorLinearLayout2);
       ll6Inf.setBackgroundResource(R.color.colorLinearLayout2);
       ll7Inf.setBackgroundResource(R.color.colorLinearLayout2);
       cvAdPhoto.setVisibility(View.GONE);
   }

    ////////
    private void galeria() {
        if (ContextCompat.checkSelfPermission(InformationUserActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(InformationUserActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, CODEGALLERY);
        } else {
            Intent selectIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            selectIntent.setType("image/*");
            startActivityForResult(selectIntent, CODEGALLERY);
            //cvAdPhoto.setVisibility(View.GONE);
        }
    }

    public void camara() {
        if (ContextCompat.checkSelfPermission(InformationUserActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(InformationUserActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(InformationUserActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(InformationUserActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 444);
        } else {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.getLocalizedMessage();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(InformationUserActivity.this, "com.stardust.lubogo.fileprovider", photoFile);
                Intent takeIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                takeIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takeIntent, CODECAMERA);
            }
        }
    }

    public File createImageFile() throws IOException {
        File dir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString());

        return new File(dir, filename + fileFormat);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CODEGALLERY:
                    if (resultCode == RESULT_OK && data != null) {
                        Log.d("Image_data", data.toString());
                        if (data.getData() != null) {
                            Log.d("Image_url", data.getData().toString());
                            Uri imageUri = data.getData();
                            chargeImage(imageUri);
                            civToShowPhotoUser.setImageURI(data.getData());
                            removeMessageAddPhoto();


                        } else {
                            Toast.makeText(this, "Error cargando imagen", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case CODECAMERA:
                    if (resultCode == RESULT_OK) {
                        chargeImageCaptured();
                    } else {
                        Toast.makeText(this, "Error cargando imagen", Toast.LENGTH_SHORT).show();
                    }
                    break;

            }
        }
    }

    private void chargeImage(Uri imageUri) {
        String[] filePath = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(imageUri, filePath, null, null, null);
        cursor.moveToFirst();
        String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap b = BitmapFactory.decodeFile(imagePath, options);
        b = changeBitmapOrientation(b, imagePath);
        float porcentage = ((400*100) / b.getWidth());
        float newHeight =  ((porcentage * b.getHeight())/100);
        //b = b.getWidth() > 1024 ? getResizedBitmap(b,800,  newHeight) : b;
        //b = Bitmap.createScaledBitmap(b,(int)(b.getWidth()*0.😎, (int)(b.getHeight()*0.😎, true);
        b = Bitmap.createScaledBitmap(b,400,(int) newHeight, true);

        File dir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString());
        // tvImageSelected.setText(getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString()+filename);
        dir.mkdirs();
        File file = new File(dir, filename + fileFormat);

        try {
            FileOutputStream f = new FileOutputStream(file);
            b.compress(Bitmap.CompressFormat.JPEG, 100, f);
            f.flush();
            f.close();

            fileUpload = file;
            dirUpload = dir;

            new uploadImageFilesTask().execute();

        } catch (FileNotFoundException e) {
            e.getLocalizedMessage();
        } catch (IOException e) {
            e.getLocalizedMessage();
        }

        cursor.close();
    }

    private void chargeImageCaptured() {
        File dir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString());
        File file = new File(dir, filename + fileFormat);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap b = BitmapFactory.decodeFile(file.getPath(), options);
        b = changeBitmapOrientation(b, file.getPath());
        float porcentage = ((600*100) / b.getWidth());
        float newHeight =  ((porcentage * b.getHeight())/100);
        //  b = b.getWidth() > 1024 ? getResizedBitmap(b,800,  newHeight) : b;
        //  b = Bitmap.createScaledBitmap(b,(int)(b.getWidth()*0.😎, (int)(b.getHeight()*0.😎, true);
        b = Bitmap.createScaledBitmap(b,600,(int) newHeight, true);
        //  ivPhoto.setImageBitmap(b);
        getResources().getColor(R.color.colorAccent);
        try {
            FileOutputStream f = new FileOutputStream(file);
            b.compress(Bitmap.CompressFormat.JPEG, 100, f);
            f.flush();
            f.close();
            fileUpload = file;
            dirUpload = dir;

            new uploadImageFilesTask().execute();

        } catch (FileNotFoundException e) {
            e.getLocalizedMessage();
        } catch (IOException e) {
            e.getLocalizedMessage();
        }
    }

    public Bitmap changeBitmapOrientation(Bitmap bm, String path) throws NullPointerException {
        if (bm != null) {
            int width = bm.getWidth();
            int height = bm.getHeight();
            int exif = 0;
            try {
                ExifInterface exifInterface = new ExifInterface(path);
                exif = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Matrix matrix = new Matrix();

            switch (exif) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    break;
            }

            return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        } else {
            return bm;
        }
    }

    private class uploadImageFilesTask extends AsyncTask<Void,Integer,Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... voids) {
            uploadFile();
            try {
                Thread.sleep(1100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                }
            }, 1000);
        }
    }

    private void uploadFile() {
        try {
            File dir = new File(InformationUserActivity.this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString());
            final File file = new File(dir, filename+fileFormat);
            final String uploadId = UUID.randomUUID().toString();
            new MultipartUploadRequest(InformationUserActivity.this, uploadId, ("http://go.lubo.com.mx/back-lubogo/public/image/addPhotoUser/" + prefs.getUserId()))
                    .addFileToUpload(file.toString(), "img")
                    .setMaxRetries(4)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onProgress(Context context, UploadInfo uploadInfo) {

                        }
                        @Override
                        public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {
                            Toast.makeText(context, serverResponse.getBody().toString(), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {
                            Toast.makeText(context, "Imagen agregada", Toast.LENGTH_SHORT).show();
                            /**  createDialog();
                             //Toasty.custom(CompleteRequestActivity.this, "Servicio solicitado", getResources().getDrawable(R.drawable.ic_ok_toast), getResources().getColor(R.color.colorYellowTitle), Toast.LENGTH_SHORT, true, true).show();
                             btnCamera.setVisibility(View.GONE);
                             btnGallery.setVisibility(View.GONE);
                             viewPhoto.setVisibility(View.GONE);*/
                        }

                        @Override
                        public void onCancelled(Context context, UploadInfo uploadInfo) {
                            Toast.makeText(context, "Cancelado", Toast.LENGTH_SHORT).show();
                            // Toasty.custom(CompleteRequestActivity.this, "Cancelado", getResources().getDrawable(R.drawable.ic_close_black_toast), getResources().getColor(R.color.colorTextBlack), Toast.LENGTH_SHORT, true, true).show();

                        }
                    }).startUpload();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
