package com.startdust.lubogo.activity.recoverPassword;

import android.app.Activity;

import com.startdust.lubogo.retrofit.RetrofitModule;

public class RecoverPasswordInteractor {
    Activity activity;
    RecoverPasswordMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;

    public RecoverPasswordInteractor(Activity activity, RecoverPasswordMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }
}
