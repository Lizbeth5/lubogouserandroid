package com.startdust.lubogo.retrofit.model;

import java.util.ArrayList;

public class GetListScheduleServiceEstablishmentResult {
    ArrayList<GetListScheduleServiceEstablishmentData> Data;

    public GetListScheduleServiceEstablishmentResult() {
    }

    public GetListScheduleServiceEstablishmentResult(ArrayList<GetListScheduleServiceEstablishmentData> data) {
        Data = data;
    }

    public ArrayList<GetListScheduleServiceEstablishmentData> getData() {
        return Data;
    }

    public void setData(ArrayList<GetListScheduleServiceEstablishmentData> data) {
        Data = data;
    }
}
