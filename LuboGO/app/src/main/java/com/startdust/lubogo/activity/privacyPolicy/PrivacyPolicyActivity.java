package com.startdust.lubogo.activity.privacyPolicy;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;

public class PrivacyPolicyActivity extends AppCompatActivity {
    WebView view;
    ImageView ivReturnLegal;

    @Override
    public void onCreate(Bundle savedIntanceState){
        super.onCreate(savedIntanceState);
        setContentView(R.layout.activity_privacy_policy);

        ivReturnLegal = findViewById(R.id.ivReturnLegal1);
        view = findViewById(R.id.wvPoliticaPrivacidad);

        view.loadUrl("http://lubo.com.mx/privacidad/privacidad-movil.html");

        ivReturnLegal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
