package com.startdust.lubogo.ui.saved;

import com.startdust.lubogo.retrofit.RetrofitAPI;
import com.startdust.lubogo.retrofit.model.GetListProductsSavedData;
import com.startdust.lubogo.retrofit.model.PutDeleteProductSavedResponse;

import java.util.ArrayList;

public interface SavedMVP {

    interface ViewMVP{
        void onSuccessListProductsSaved(ArrayList<GetListProductsSavedData> getListProductsSavedData);
        void onSuccessDeleteProductSaved(PutDeleteProductSavedResponse putDeleteProductSavedResponse);
        void onError(String error);
    }

    interface PresenterMVP{
        void onSuccessListProductsSaved(ArrayList<GetListProductsSavedData> getListProductsSavedData);
        void onSuccessDeleteProductSaved(PutDeleteProductSavedResponse putDeleteProductSavedResponse);
        void onError(String error);
    }
}
