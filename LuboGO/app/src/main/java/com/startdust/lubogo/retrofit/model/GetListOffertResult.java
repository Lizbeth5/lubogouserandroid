package com.startdust.lubogo.retrofit.model;

public class GetListOffertResult {
    String idOferta;
    String idProducto;
    String marca;
    String descripcion;
    String calificacion;
    String precio;
    String urlFoto;
    String stock;
    String idEstablecimiento;
    String nombre;

    public GetListOffertResult() {
    }

    public GetListOffertResult(String idOferta, String idProducto, String marca, String descripcion, String calificacion, String precio, String urlFoto, String stock, String idEstablecimiento, String nombre) {
        this.idOferta = idOferta;
        this.idProducto = idProducto;
        this.marca = marca;
        this.descripcion = descripcion;
        this.calificacion = calificacion;
        this.precio = precio;
        this.urlFoto = urlFoto;
        this.stock = stock;
        this.idEstablecimiento = idEstablecimiento;
        this.nombre = nombre;
    }

    public String getIdOferta() {
        return idOferta;
    }

    public void setIdOferta(String idOferta) {
        this.idOferta = idOferta;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public void setIdEstablecimiento(String idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
