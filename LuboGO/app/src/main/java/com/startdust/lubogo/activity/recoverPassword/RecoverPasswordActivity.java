package com.startdust.lubogo.activity.recoverPassword;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;

public class RecoverPasswordActivity extends AppCompatActivity {
    ImageView ivCloseRecoverPassword;
    EditText etEnterEmail;
    TextView tvSubmitEmail;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover_password);

        ivCloseRecoverPassword = findViewById(R.id.ivCloseRecoverPassword);
        etEnterEmail = findViewById(R.id.etEnterEmail);
        tvSubmitEmail = findViewById(R.id.tvSubmitEmail);

        ivCloseRecoverPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
