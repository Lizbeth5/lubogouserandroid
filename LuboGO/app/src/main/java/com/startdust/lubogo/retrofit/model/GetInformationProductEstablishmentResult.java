package com.startdust.lubogo.retrofit.model;

public class GetInformationProductEstablishmentResult {
    String idProducto;
    String producto;
    String marca;
    String descripcion;
    String calificacion;
    String precio;
    String urlFoto;
    String descuento;
    String stock;
    String fechaCaducidad;
    String idSubCategorias;
    String idStatusProducto;
    String idEstablecimiento;

    public GetInformationProductEstablishmentResult() {
    }

    public GetInformationProductEstablishmentResult(String idProducto, String producto, String marca, String descripcion, String calificacion, String precio, String urlFoto, String descuento, String stock, String fechaCaducidad, String idSubCategorias, String idStatusProducto, String idEstablecimiento) {
        this.idProducto = idProducto;
        this.producto = producto;
        this.marca = marca;
        this.descripcion = descripcion;
        this.calificacion = calificacion;
        this.precio = precio;
        this.urlFoto = urlFoto;
        this.descuento = descuento;
        this.stock = stock;
        this.fechaCaducidad = fechaCaducidad;
        this.idSubCategorias = idSubCategorias;
        this.idStatusProducto = idStatusProducto;
        this.idEstablecimiento = idEstablecimiento;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(String fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    public String getIdSubCategorias() {
        return idSubCategorias;
    }

    public void setIdSubCategorias(String idSubCategorias) {
        this.idSubCategorias = idSubCategorias;
    }

    public String getIdStatusProducto() {
        return idStatusProducto;
    }

    public void setIdStatusProducto(String idStatusProducto) {
        this.idStatusProducto = idStatusProducto;
    }

    public String getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public void setIdEstablecimiento(String idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }
}

