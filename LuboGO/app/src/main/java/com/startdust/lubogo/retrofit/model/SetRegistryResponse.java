package com.startdust.lubogo.retrofit.model;

public class SetRegistryResponse {
    Boolean response;
    String result;
    String message;
    String errors;

    public SetRegistryResponse() {

    }

    public SetRegistryResponse(Boolean response, String result, String message, String errors) {
        this.response = response;
        this.result = result;
        this.message = message;
        this.errors = errors;
    }

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }
}
