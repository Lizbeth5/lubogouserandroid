package com.startdust.lubogo.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.cuztomizerOrder.CustomizerOrderActivity;
import com.startdust.lubogo.retrofit.model.GetListProductEstablishmentResult;

import java.util.ArrayList;

public class MenuEstablishmentAdapter extends RecyclerView.Adapter<MenuEstablishmentAdapter.ViewHolder> {
    Context context;
    ArrayList<GetListProductEstablishmentResult> data;

    public MenuEstablishmentAdapter(Context context, ArrayList<GetListProductEstablishmentResult> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public MenuEstablishmentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_menu_establishment, parent,false);
        return new MenuEstablishmentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuEstablishmentAdapter.ViewHolder holder, int position) {
        holder.Bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{
        TextView tvNameProductMenu, tvDescriptionProductMenu,tvPriceProductMenu;
        ImageView ivImageProductMenu;
        RelativeLayout rlProductMenu;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvNameProductMenu = itemView.findViewById(R.id.tvNameProductMenu);
            tvDescriptionProductMenu = itemView.findViewById(R.id.tvDescriptionProductMenu);
            tvPriceProductMenu = itemView.findViewById(R.id.tvPriceProductMenu);
            ivImageProductMenu = itemView.findViewById(R.id.ivImageProductMenu);
            rlProductMenu = itemView.findViewById(R.id.rlProductMenu);
        }

        public void Bind(final GetListProductEstablishmentResult getListProductEstablishmentResult){
            Log.i("producto", getListProductEstablishmentResult.getProducto());
            tvNameProductMenu.setText(getListProductEstablishmentResult.getProducto());
            tvDescriptionProductMenu.setText(getListProductEstablishmentResult.getDescripcion());
            tvPriceProductMenu.setText("$ " + getListProductEstablishmentResult.getPrecio());

            String downloadImage =  getListProductEstablishmentResult.getUrlFoto();
            Picasso.with(context)
                    .load(downloadImage)
                    .into(ivImageProductMenu);

            rlProductMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CustomizerOrderActivity.class);
                    intent.putExtra("idProducto", getListProductEstablishmentResult.getIdProducto());
                    context.startActivity(intent);
                }
            });
        }
    }
}
