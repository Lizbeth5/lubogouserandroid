package com.startdust.lubogo.fragment.menu;

import android.app.Activity;

import com.startdust.lubogo.retrofit.model.GetListProductEstablishmentResult;

import java.util.ArrayList;

public class MenuTabFragmentPresenter implements MenuTabFragmentMVP.PresenterMVP {
    Activity activity;
    MenuTabFragmentMVP.ViewMVP viewMVP;
    MenuTabFragmentInteractor interactor;

    public MenuTabFragmentPresenter(Activity activity, MenuTabFragmentMVP.ViewMVP viewMVP) {
        this.activity = activity;
        this.viewMVP = viewMVP;

        interactor = new MenuTabFragmentInteractor(activity,this);
    }

    @Override
    public void onSuccessListProducts(ArrayList<GetListProductEstablishmentResult> getListProductEstablishmentResults) {
        viewMVP.onSuccessListProducts(getListProductEstablishmentResults);
    }

    @Override
    public void onError(String error) {
        viewMVP.onError(error);
    }

    public void listProductsEstablishment(String id){
        interactor.listProductsEstablishment(id);
    }
}
