package com.startdust.lubogo.fragment.menu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.startdust.lubogo.R;
import com.startdust.lubogo.adapter.MenuEstablishmentAdapter;
import com.startdust.lubogo.retrofit.model.GetListProductEstablishmentResult;

import java.util.ArrayList;

public class MenuTabFragment extends Fragment implements MenuTabFragmentMVP.ViewMVP{
    RecyclerView rvListMenuEstablishment;

    MenuTabFragmentPresenter presenter;
    LinearLayoutManager linearLayoutManager;
    MenuEstablishmentAdapter adapter;
    String idProducto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tab_menu, container, false);

        rvListMenuEstablishment = root.findViewById(R.id.rvListMenuEstablishment);

        linearLayoutManager = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        presenter = new MenuTabFragmentPresenter(getActivity(),this);

        idProducto = getActivity().getIntent().getExtras().getString("idEstablecimiento");

       presenter.listProductsEstablishment(idProducto);

        return root;
    }

    @Override
    public void onSuccessListProducts(ArrayList<GetListProductEstablishmentResult> getListProductEstablishmentResults) {
        adapter = new MenuEstablishmentAdapter(getContext(),getListProductEstablishmentResults);
        rvListMenuEstablishment.setAdapter(adapter);
        rvListMenuEstablishment.setLayoutManager(linearLayoutManager);

    }

    @Override
    public void onError(String error) {

    }
}
