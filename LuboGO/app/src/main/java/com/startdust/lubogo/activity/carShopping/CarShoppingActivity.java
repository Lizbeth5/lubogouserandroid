package com.startdust.lubogo.activity.carShopping;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.methodPayment.MethodPaymentActivity;

public class CarShoppingActivity extends AppCompatActivity {
    CardView cvSearchList;
    ImageView ivCloseCarShopping, ivImageMethodPayment;
    TextView tvShowNameEstablishment1, tvShowAddressDestination, tvAddNoteDelivery, tvShowQuantityOrders, tvShowNameProduct,tvShowPriceProducto,
             tvShowSubTotal, tvShowShippingCost, tvShowTotal, tvShowTotalAmount, tvOpenMethodPayment;
    RelativeLayout rlConfirmPayment;
    RecyclerView rvListProductsCar;


    @Override
    public void onCreate(Bundle savedIntanceState){
        super.onCreate(savedIntanceState);
        setContentView(R.layout.activity_car_shopping);

        cvSearchList = findViewById(R.id.cvSearchList);
        ivCloseCarShopping = findViewById(R.id.ivCloseCarShopping);
        tvShowNameEstablishment1 = findViewById(R.id.tvShowNameEstablishment1);
        tvShowAddressDestination = findViewById(R.id.tvShowAddressDestination);
        tvAddNoteDelivery = findViewById(R.id.tvAddNoteDelivery);
        tvShowQuantityOrders = findViewById(R.id.tvShowQuantityOrders);
        tvShowNameProduct = findViewById(R.id.tvShowNameProduct);
        tvShowPriceProducto = findViewById(R.id.tvShowPriceProducto);
        rvListProductsCar = findViewById(R.id.rvListProductsCar);
        ivImageMethodPayment = findViewById(R.id.ivImageMethodPayment);
        tvShowSubTotal = findViewById(R.id.tvShowSubTotal);
        tvShowShippingCost = findViewById(R.id.tvShowShippingCost);
        tvShowTotal = findViewById(R.id.tvShowTotal);
        tvShowTotalAmount = findViewById(R.id.tvShowTotalAmount);
        rlConfirmPayment = findViewById(R.id.rlConfirmPayment);
        tvOpenMethodPayment =findViewById(R.id.tvOpenMethodPayment);

        ivCloseCarShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvOpenMethodPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CarShoppingActivity.this, MethodPaymentActivity.class);
                startActivity(intent);
            }
        });
    }
}
