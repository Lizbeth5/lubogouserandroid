package com.startdust.lubogo.activity.profileEstablishment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;
import com.startdust.lubogo.R;
import com.startdust.lubogo.adapter.TabLayoutAdapter;
import com.startdust.lubogo.activity.carShopping.CarShoppingActivity;
import com.startdust.lubogo.activity.moreInformation.MoreInformationActivity;
import com.startdust.lubogo.retrofit.model.GetInformationProfileEstablishmentResult;
import com.startdust.lubogo.retrofit.model.GetListProductEstablishmentResult;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileEstablishmentActivity extends AppCompatActivity implements ProfileEstablishmentMVP.ViewMVP {
    TabLayout tabLayout;
    ViewPager viewPager;

    ImageView ivPhotoEstablishment, ivCloseProfileEstablishment;
    CircleImageView civLogoEstablishment;
    TextView tvNameEstablishment, tvTypeEstblishment, tvTimeDear, tvQualificationEstablishment, tvQuantity,
             tvEstimatedCost, tvSeeMoreInformationEstablishment, tvToShowAddressEstablishment;
    RelativeLayout rlSeeCarShopping;

    String idEstablecimiento;
    ProfileEstablishmentPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_establishment);

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        ivPhotoEstablishment = findViewById(R.id.ivPhotoEstablishment);
        ivCloseProfileEstablishment = findViewById(R.id.ivCloseProfileEstablishment);
        civLogoEstablishment = findViewById(R.id.cviLogoEstablishment);
        tvNameEstablishment = findViewById(R.id.tvNameEstablishment);
        tvTypeEstblishment = findViewById(R.id.tvTypeEstblishment);
        tvTimeDear = findViewById(R.id.tvTimeDear);
        tvQualificationEstablishment = findViewById(R.id.tvQualificationEstablishment);
        tvQuantity = findViewById(R.id.tvQuantity);
        tvEstimatedCost = findViewById(R.id.tvEstimatedCost);
        tvToShowAddressEstablishment = findViewById(R.id.tvToShowAddressEstablishment);
        tvSeeMoreInformationEstablishment = findViewById(R.id.tvSeeMoreInformationEstablishment);
        rlSeeCarShopping = findViewById(R.id.rlSeeCarShopping);


        presenter = new ProfileEstablishmentPresenter(this,this);
        idEstablecimiento = getIntent().getExtras().getString("idEstablecimiento");

        presenter.informationEstablishment(idEstablecimiento);

        tabLayout.addTab(tabLayout.newTab().setText("Menú"));
        //tabLayout.addTab(tabLayout.newTab().setText("Entradas"));
        //tabLayout.addTab(tabLayout.newTab().setText("Cenas"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        final TabLayoutAdapter adapter = new TabLayoutAdapter(this,getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        ivCloseProfileEstablishment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onSuccessInformationEstablishment(final GetInformationProfileEstablishmentResult getInformationProfileEstablishmentResult) {
        //lleno cada uno de los elementos del xml
        Log.i("informacion perfil", getInformationProfileEstablishmentResult.getNombre());
        tvNameEstablishment.setText(getInformationProfileEstablishmentResult.getNombre());
        tvTypeEstblishment.setText("Restaurante");//getInformationProfileEstablishmentResult.getIdEstablecimiento());
        tvQualificationEstablishment.setText(getInformationProfileEstablishmentResult.getCalificacion());
        tvToShowAddressEstablishment.setText(getInformationProfileEstablishmentResult.getDireccion());
        tvEstimatedCost.setText("MXN " + getInformationProfileEstablishmentResult.getPrecioEnvio());

        String downloadPhoto = getInformationProfileEstablishmentResult.getUrlFoto();
        Picasso.with(this)
                .load(downloadPhoto)
                .into(ivPhotoEstablishment);

        String downloadLogo = getInformationProfileEstablishmentResult.getUrlImageLogo();
        Picasso.with(this)
                .load(downloadLogo)
                .into(civLogoEstablishment);

        tvSeeMoreInformationEstablishment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileEstablishmentActivity.this, MoreInformationActivity.class);
                intent.putExtra("idEstablecimiento", getInformationProfileEstablishmentResult.getIdEstablecimiento());
                startActivity(intent);
            }
        });

        rlSeeCarShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileEstablishmentActivity.this, CarShoppingActivity.class);
                intent.putExtra("idEstablecimiento", getInformationProfileEstablishmentResult.getIdEstablecimiento());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onSuccessListProductsEstablishment(ArrayList<GetListProductEstablishmentResult> getListProductEstablishmentResults) {

    }

    @Override
    public void onError(String error) {

    }
}
