package com.startdust.lubogo.ui.home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.startdust.lubogo.R;
import com.startdust.lubogo.adapter.CategoriesAdapter;
import com.startdust.lubogo.adapter.OffertsAdapter;
import com.startdust.lubogo.activity.carShopping.CarShoppingActivity;
import com.startdust.lubogo.activity.category.CategoryActivity;
import com.startdust.lubogo.preferences.AppPreferences;
import com.startdust.lubogo.retrofit.model.GetListCategoriesData;
import com.startdust.lubogo.retrofit.model.GetListOffertResult;
import com.startdust.lubogo.retrofit.model.PostSavedProductResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HomeFragment extends Fragment implements HomeMVP.ViewMVP{
    ImageView ivCarShopping;
    private HomeViewModel homeViewModel;
    CardView cvCategoryRestaurant;
    TextView tvUbication;
    RecyclerView rvListCategories, rvListOffers;

    HomePresenter presenter;
    LinearLayoutManager linearLayoutManager, linearLayoutManager2;
    CategoriesAdapter adapterCategories;
    OffertsAdapter adapterOfferts;
    String idProducto;
    AppPreferences prefs;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        ivCarShopping = root.findViewById(R.id.ivCarShopping);
        cvCategoryRestaurant = root.findViewById(R.id.cvCategoryRestaurant);
        tvUbication = root.findViewById(R.id.tvUbication);
        rvListCategories = root.findViewById(R.id.rvListCategories);
        rvListOffers = root.findViewById(R.id.rvListOffers);

        //idProducto = getActivity().getIntent().getExtras().getString("idProducto");
        HomeFragment fragment = new HomeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("idProducto", idProducto);
        fragment.setArguments(bundle);//Here pass your data


        prefs = new AppPreferences(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false);
        linearLayoutManager2 = new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false);

        presenter = new HomePresenter(getActivity(),this);

        ivCarShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CarShoppingActivity.class);
                startActivity(intent);
            }
        });

        cvCategoryRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CategoryActivity.class);
                startActivity(intent);
            }
        });

        //permisos
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationStart();
        }

        presenter.listOfferts();
        presenter.listCategories();
        presenter.savedProduct(prefs.getUserId(), idProducto,"1");

        return root;
    }

    @Override
    public void onSuccesListOffert(ArrayList<GetListOffertResult> getListOffertResults) {
        Log.i("oferta", getListOffertResults.get(0).getIdOferta());
        adapterOfferts = new OffertsAdapter(getContext(),getListOffertResults);
        rvListOffers.setLayoutManager(linearLayoutManager);
        rvListOffers.setAdapter(adapterOfferts);
    }

    @Override
    public void onSuccesListCategories(ArrayList<GetListCategoriesData> getListCategoriesData){
        Log.i("categoria", getListCategoriesData.get(0).getDescripcion());
        adapterCategories = new CategoriesAdapter(getContext(),getListCategoriesData);
        rvListCategories.setLayoutManager(linearLayoutManager2);
        rvListCategories.setAdapter(adapterCategories);
    }

    @Override
    public void onSuccesSavedProduct(PostSavedProductResponse postSavedProductResponse) {
        //Toast.makeText(getContext(), "Guardado.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String error) {

    }

    private void locationStart() {
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        Localizacion localizacion = new Localizacion();
        localizacion.setMainActivity(this);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!gpsEnabled) {
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) localizacion);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) localizacion);
        //tvUbication.setText("");
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationStart();
                return;
            }
        }
    }

    public void setLocation(Location location) {
        //Obtener la direccion de la calle a partir de la latitud y la longitud
        if (location.getLatitude() != 0.0 && location.getLongitude() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address addresStreet = list.get(0);
                    tvUbication.setText(addresStreet.getAddressLine(0));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //Aqui empieza la Clase Localizacion
    public class Localizacion implements LocationListener {
        HomeFragment homeFragment;

        public HomeFragment getHomeFragment() {
            return homeFragment;
        }

        public void setMainActivity(HomeFragment homeFragment) {
            this.homeFragment = homeFragment;
        }

        @Override
        public void onLocationChanged(Location location) {
            //Cuando la ubicacion ha cambiado este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas debido a la deteccion de un cambio de ubicacion
            setLocation(location);
            this.homeFragment.setLocation(location);
        }

        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado
        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            //Este método fue desaprobado en el nivel 29 de API. Esta devolución de llamada nunca se invocará.
        }
    }
}