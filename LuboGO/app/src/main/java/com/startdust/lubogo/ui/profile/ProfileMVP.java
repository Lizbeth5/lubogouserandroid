package com.startdust.lubogo.ui.profile;

import com.startdust.lubogo.retrofit.model.GetInformationUserResult;

public interface ProfileMVP {
    interface ViewMVP{
        void onSuccessProfile(GetInformationUserResult getInformationUserResult);
        void onError(String error);

    }

    interface PresenterMVP{
        void onSuccessProfile(GetInformationUserResult getInformationUserResult);
        void onError(String error);
    }

}
