package com.startdust.lubogo.model;

public class OffertsModel {
    String urlImage;
    String nameEstablishment;
    String typeEstablishment;
    float qualification;
    String time;
    String cost;
    int orders;

    public OffertsModel(){

    }

    public OffertsModel(String urlImage, String nameEstablishment, String typeEstablishment, float qualification, String time, String cost, int orders) {
        this.urlImage = urlImage;
        this.nameEstablishment = nameEstablishment;
        this.typeEstablishment = typeEstablishment;
        this.qualification = qualification;
        this.time = time;
        this.cost = cost;
        this.orders = orders;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getNameEstablishment() {
        return nameEstablishment;
    }

    public void setNameEstablishment(String nameEstablishment) {
        this.nameEstablishment = nameEstablishment;
    }

    public String getTypeEstablishment() {
        return typeEstablishment;
    }

    public void setTypeEstablishment(String typeEstablishment) {
        this.typeEstablishment = typeEstablishment;
    }

    public float getQualification() {
        return qualification;
    }

    public void setQualification(float qualification) {
        this.qualification = qualification;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public int getOrders() {
        return orders;
    }

    public void setOrders(int orders) {
        this.orders = orders;
    }
}
