package com.startdust.lubogo.activity.email;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;

public class EditEmailActivity extends AppCompatActivity {
    ImageView ivReturnInformationUser;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_email);

        ivReturnInformationUser = findViewById(R.id.ivReturnInformationUser);

        ivReturnInformationUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
