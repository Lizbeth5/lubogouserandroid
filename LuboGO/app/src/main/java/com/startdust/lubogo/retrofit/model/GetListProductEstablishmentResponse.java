package com.startdust.lubogo.retrofit.model;

import java.util.ArrayList;

public class GetListProductEstablishmentResponse {
    Boolean response;
    String message;
    String errors;
    ArrayList<GetListProductEstablishmentResult> result;

    public GetListProductEstablishmentResponse() {
    }

    public GetListProductEstablishmentResponse(Boolean response, String message, String errors, ArrayList<GetListProductEstablishmentResult> result) {
        this.response = response;
        this.message = message;
        this.errors = errors;
        this.result = result;
    }

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public ArrayList<GetListProductEstablishmentResult> getResult() {
        return result;
    }

    public void setResult(ArrayList<GetListProductEstablishmentResult> result) {
        this.result = result;
    }
}
