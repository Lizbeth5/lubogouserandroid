package com.startdust.lubogo.activity.informationUser;

import android.app.Activity;
import android.util.Log;

import com.startdust.lubogo.preferences.AppPreferences;
import com.startdust.lubogo.retrofit.RetrofitModule;
import com.startdust.lubogo.retrofit.model.GetInformationUserResponse;
import com.startdust.lubogo.retrofit.model.PutUpdateInformationResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InformationUserInteractor {
    Activity activity;
    InformationUserMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;
    Call<GetInformationUserResponse> getInformationUserResponseCall;
    Call<PutUpdateInformationResponse> putUpdateInformationResponseCall;
    AppPreferences prefs;

    public InformationUserInteractor(Activity activity, InformationUserMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
        prefs = new AppPreferences(activity);
    }

    public void informationUser(){
        Log.i("idUsuario", prefs.getUserId());
        getInformationUserResponseCall = retrofitModule.getInformationUser().getInformationUser(prefs.getUserId());
        getInformationUserResponseCall.enqueue(new Callback<GetInformationUserResponse>() {
            @Override
            public void onResponse(Call<GetInformationUserResponse> call, Response<GetInformationUserResponse> response) {
                if(response.isSuccessful()){
                    presenterMVP.onSuccessInformationUser(response.body().getResult());
                }else{
                    presenterMVP.onError("Error al mostrar información");
                }
            }

            @Override
            public void onFailure(Call<GetInformationUserResponse> call, Throwable t) {
                Log.i("nombre error", t.getMessage());
            }
        });
    }

    public void updateInformation(String name, String lastName){
        putUpdateInformationResponseCall = retrofitModule.putUpdateInformation().putUpdateInformation(prefs.getUserId(), name, lastName);
        putUpdateInformationResponseCall.enqueue(new Callback<PutUpdateInformationResponse>() {
            @Override
            public void onResponse(Call<PutUpdateInformationResponse> call, Response<PutUpdateInformationResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getResponse()){
                        presenterMVP.onSuccessUpdateInformation();
                    }
                }else{
                    presenterMVP.onError("Error al actualizar");
                }
            }

            @Override
            public void onFailure(Call<PutUpdateInformationResponse> call, Throwable t) {
                Log.i("Error al actualizar", t.getMessage());
            }
        });
    }
}
