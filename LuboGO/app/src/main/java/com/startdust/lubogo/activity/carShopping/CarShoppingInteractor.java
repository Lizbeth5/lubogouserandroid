package com.startdust.lubogo.activity.carShopping;

import android.app.Activity;

import com.startdust.lubogo.retrofit.RetrofitModule;

public class CarShoppingInteractor {
    Activity activity;
    CarShoppingMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;

    public CarShoppingInteractor(Activity activity, CarShoppingMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }
}
