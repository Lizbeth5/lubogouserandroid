package com.startdust.lubogo.ui.saved;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.home.MainActivity;
import com.startdust.lubogo.adapter.SavedAdapter;
import com.startdust.lubogo.preferences.AppPreferences;
import com.startdust.lubogo.retrofit.model.GetListProductsSavedData;
import com.startdust.lubogo.retrofit.model.PutDeleteProductSavedResponse;

import java.util.ArrayList;

public class SavedFragment extends Fragment implements SavedMVP.ViewMVP{
    CardView cvSearch;
    RecyclerView rvListProductsSaved;

    SavedPresenter presenter;
    AppPreferences prefs;
    SavedAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    SavedViewModel savedViewModel;
    String idProducto;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        savedViewModel = ViewModelProviders.of(this).get(SavedViewModel.class);
        View root = inflater.inflate(R.layout.fragment_saved, container, false);

        cvSearch = root.findViewById(R.id.cvSearch);
        rvListProductsSaved = root.findViewById(R.id.rvListProductsSaved);

        //idProducto = getActivity().getIntent().getExtras().getString("idProducto");

        SavedFragment fragment = new SavedFragment();
        Bundle bundle = new Bundle();
        bundle.putString("idProducto", idProducto);
        fragment.setArguments(bundle);//Here pass your data


        linearLayoutManager = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        presenter = new SavedPresenter(this,getActivity());
        prefs = new AppPreferences(getActivity());

        cvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });

        presenter.listProductsSaved(prefs.getUserId());
        presenter.deleteProductSaved(idProducto);
        return root;
    }

    @Override
    public void onSuccessListProductsSaved(ArrayList<GetListProductsSavedData> getListProductsSavedData) {
        adapter = new SavedAdapter(getContext(), getListProductsSavedData);
        rvListProductsSaved.setLayoutManager(linearLayoutManager);
        rvListProductsSaved.setAdapter(adapter);
    }

    @Override
    public void onSuccessDeleteProductSaved(PutDeleteProductSavedResponse putDeleteProductSavedResponse) {
        //Toast.makeText(getContext(), "Eliminado de tus Guardado.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String error) {

    }
}