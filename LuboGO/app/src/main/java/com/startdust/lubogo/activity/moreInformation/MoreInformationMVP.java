package com.startdust.lubogo.activity.moreInformation;

import com.startdust.lubogo.retrofit.model.GetInformationProfileEstablishmentResult;
import com.startdust.lubogo.retrofit.model.GetListScheduleServiceEstablishmentData;

import java.util.ArrayList;

public interface MoreInformationMVP {
    interface ViewMVP{
        void onSuccessInformationProfile(GetInformationProfileEstablishmentResult getInformationProfileEstablishmentResult);
        void onSuccessListScheduleServiceEstablishment(ArrayList<GetListScheduleServiceEstablishmentData> getListScheduleServiceEstablishmentData);
        void onError(String error);
    }
    interface PresenterMVP{
        void onSuccessInformationProfile(GetInformationProfileEstablishmentResult getInformationProfileEstablishmentResult);
        void onSuccessListScheduleServiceEstablishment(ArrayList<GetListScheduleServiceEstablishmentData> getListScheduleServiceEstablishmentData);
        void onError(String error);
    }
}
