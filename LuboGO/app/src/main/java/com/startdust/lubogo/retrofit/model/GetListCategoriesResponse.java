package com.startdust.lubogo.retrofit.model;

public class GetListCategoriesResponse {
    Boolean response;
    String message;
    String errors;
    GetListCategoriesResult result;

    public GetListCategoriesResponse() {
    }

    public GetListCategoriesResponse(Boolean response, String message, String errors, GetListCategoriesResult result) {
        this.response = response;
        this.message = message;
        this.errors = errors;
        this.result = result;
    }

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public GetListCategoriesResult getResult() {
        return result;
    }

    public void setResult(GetListCategoriesResult result) {
        this.result = result;
    }
}
