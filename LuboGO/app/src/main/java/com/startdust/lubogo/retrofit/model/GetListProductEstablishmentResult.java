package com.startdust.lubogo.retrofit.model;

import java.util.ArrayList;

public class GetListProductEstablishmentResult {
    String idProducto;
    String producto;
    String marca;
    String descripcion;
    float calificacion;
    float precio;
    String urlFoto;
    float descuento;
    int stock;
    String fechaCaducidad;
    String idSubCategorias;
    String idStatusProducto;
    String idEstablecimiento;

    public GetListProductEstablishmentResult() {
    }

    public GetListProductEstablishmentResult(String idProducto, String producto, String marca, String descripcion, float calificacion, float precio, String urlFoto, float descuento, int stock, String fechaCaducidad, String idSubCategorias, String idStatusProducto, String idEstablecimiento) {
        this.idProducto = idProducto;
        this.producto = producto;
        this.marca = marca;
        this.descripcion = descripcion;
        this.calificacion = calificacion;
        this.precio = precio;
        this.urlFoto = urlFoto;
        this.descuento = descuento;
        this.stock = stock;
        this.fechaCaducidad = fechaCaducidad;
        this.idSubCategorias = idSubCategorias;
        this.idStatusProducto = idStatusProducto;
        this.idEstablecimiento = idEstablecimiento;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(float calificacion) {
        this.calificacion = calificacion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public float getDescuento() {
        return descuento;
    }

    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(String fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    public String getIdSubCategorias() {
        return idSubCategorias;
    }

    public void setIdSubCategorias(String idSubCategorias) {
        this.idSubCategorias = idSubCategorias;
    }

    public String getIdStatusProducto() {
        return idStatusProducto;
    }

    public void setIdStatusProducto(String idStatusProducto) {
        this.idStatusProducto = idStatusProducto;
    }

    public String getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public void setIdEstablecimiento(String idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }
}
