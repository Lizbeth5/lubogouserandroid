package com.startdust.lubogo.model;

public class CategoryModel {
    String urlImage;
    String nameCategory;

    public CategoryModel(){

    }

    public CategoryModel(String urlImage, String nameCategory) {
        this.urlImage = urlImage;
        this.nameCategory = nameCategory;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }
}
