package com.startdust.lubogo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.startdust.lubogo.R;
import com.startdust.lubogo.retrofit.model.GetListScheduleServiceEstablishmentData;

import java.util.ArrayList;

public class ScheduleServiceEstablishmentAdapter extends RecyclerView.Adapter<ScheduleServiceEstablishmentAdapter.ViewHolder> {
    Context context;
    ArrayList<GetListScheduleServiceEstablishmentData> data;

    public ScheduleServiceEstablishmentAdapter(Context context, ArrayList<GetListScheduleServiceEstablishmentData> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ScheduleServiceEstablishmentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_schedule, parent, false);
        return new ScheduleServiceEstablishmentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduleServiceEstablishmentAdapter.ViewHolder holder, int position) {
        holder.Bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvShowDays, tvShowScheduleService;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvShowDays = itemView.findViewById(R.id.tvShowDays);
            tvShowScheduleService = itemView.findViewById(R.id.tvShowScheduleService);
        }

        public void Bind(final GetListScheduleServiceEstablishmentData getListScheduleServiceEstablishmentData){
            tvShowDays.setText(getListScheduleServiceEstablishmentData.getDia());
            tvShowScheduleService.setText(getListScheduleServiceEstablishmentData.getHoraApertura() + " a " + getListScheduleServiceEstablishmentData.getHoraCierre());
        }
    }
}
