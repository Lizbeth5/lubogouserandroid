package com.startdust.lubogo.activity.verification;

import android.app.Activity;

public class VerificationPresenter implements VerificationMVP.PresenterMVP {

    VerificationMVP.ViewMVP viewInteractor;
    VerificationInteractor verificationInteractor;
    Activity activity;

    public VerificationPresenter(VerificationMVP.ViewMVP viewInteractor, Activity activity) {
        this.viewInteractor = viewInteractor;
        this.activity = activity;

        verificationInteractor = new VerificationInteractor(activity,this);
    }
}
