package com.startdust.lubogo.retrofit.model;

public class GetListScheduleServiceEstablishmentData {
    String idHorario;
    String dia;
    String horaApertura;
    String horaCierre;
    String idEstablecimiento;

    public GetListScheduleServiceEstablishmentData() {
    }

    public GetListScheduleServiceEstablishmentData(String idHorario, String dia, String horaApertura, String horaCierre, String idEstablecimiento) {
        this.idHorario = idHorario;
        this.dia = dia;
        this.horaApertura = horaApertura;
        this.horaCierre = horaCierre;
        this.idEstablecimiento = idEstablecimiento;
    }

    public String getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(String idHorario) {
        this.idHorario = idHorario;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHoraApertura() {
        return horaApertura;
    }

    public void setHoraApertura(String horaApertura) {
        this.horaApertura = horaApertura;
    }

    public String getHoraCierre() {
        return horaCierre;
    }

    public void setHoraCierre(String horaCierre) {
        this.horaCierre = horaCierre;
    }

    public String getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public void setIdEstablecimiento(String idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }
}
