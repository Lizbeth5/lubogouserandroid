package com.startdust.lubogo.activity.registry;

import android.app.Activity;

import com.startdust.lubogo.model.User;

public class RegistryPresenter implements RegistryMVP.PresenterMVP {
    RegistryInteractor registryInteractor;
    RegistryMVP.ViewMVP viewIntance;
    Activity activity;

    public RegistryPresenter(RegistryMVP.ViewMVP viewIntance, Activity activity) {
        this.viewIntance = viewIntance;
        this.activity = activity;

        registryInteractor = new RegistryInteractor(activity, this);
    }

    @Override
    public void onSuccessRegistry() {
        viewIntance.onSuccessRegistry();
    }

    @Override
    public void onError(String error) {
        viewIntance.onError(error);
    }

    public void registry(User user){
        if(user != null){
            if(user.getApellidoMaterno().isEmpty() || user.getApellidoPaterno().isEmpty() || user.getEmail().isEmpty() || user.getPassword().isEmpty() || user.getNombre().isEmpty()){
                viewIntance.onError("Llenar todos los campos");
            }else{
                registryInteractor.setRegistry(user);
            }
        }
    }
}
