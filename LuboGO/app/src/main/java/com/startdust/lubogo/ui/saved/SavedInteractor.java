package com.startdust.lubogo.ui.saved;

import android.app.Activity;

import com.startdust.lubogo.retrofit.RetrofitModule;
import com.startdust.lubogo.retrofit.model.GetListProductsSavedResponse;
import com.startdust.lubogo.retrofit.model.PutDeleteProductSavedResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SavedInteractor {
    Activity activity;
    SavedMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;
    Call<GetListProductsSavedResponse> getListProductsSavedResponseCall;
    Call<PutDeleteProductSavedResponse> putDeleteProductSavedResponseCall;

    public SavedInteractor(Activity activity, SavedMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }

    public void listProductsSaved(String id){
        getListProductsSavedResponseCall = retrofitModule.getListProductsSaved().getListProductsSaved(id);
        getListProductsSavedResponseCall.enqueue(new Callback<GetListProductsSavedResponse>() {
            @Override
            public void onResponse(Call<GetListProductsSavedResponse> call, Response<GetListProductsSavedResponse> response) {
                if(response.isSuccessful()){
                    presenterMVP.onSuccessListProductsSaved(response.body().getResult().getData());
                }else{
                    presenterMVP.onError("Error");
                }
            }

            @Override
            public void onFailure(Call<GetListProductsSavedResponse> call, Throwable t) {

            }
        });
    }

    public void deleteProductSaved(String id){
        putDeleteProductSavedResponseCall = retrofitModule.putDeleteProductSaved().putDeleteProductSaved(id);
        putDeleteProductSavedResponseCall.enqueue(new Callback<PutDeleteProductSavedResponse>() {
            @Override
            public void onResponse(Call<PutDeleteProductSavedResponse> call, Response<PutDeleteProductSavedResponse> response) {
                if(response.isSuccessful()){
                    presenterMVP.onSuccessDeleteProductSaved(response.body());
                }else{
                    presenterMVP.onError("Error.");
                }
            }

            @Override
            public void onFailure(Call<PutDeleteProductSavedResponse> call, Throwable t) {

            }
        });
    }
}
