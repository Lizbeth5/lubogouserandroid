package com.startdust.lubogo.retrofit.model;

public class GetListDetailsProductsResult {
    String idDetalleProducto;
    String idProducto;
    String Complemento;
    Double precio;

    public GetListDetailsProductsResult() {
    }

    public GetListDetailsProductsResult(String idDetalleProducto, String idProducto, String complemento, Double precio) {
        this.idDetalleProducto = idDetalleProducto;
        this.idProducto = idProducto;
        Complemento = complemento;
        this.precio = precio;
    }

    public String getIdDetalleProducto() {
        return idDetalleProducto;
    }

    public void setIdDetalleProducto(String idDetalleProducto) {
        this.idDetalleProducto = idDetalleProducto;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getComplemento() {
        return Complemento;
    }

    public void setComplemento(String complemento) {
        Complemento = complemento;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}
