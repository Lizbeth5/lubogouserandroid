package com.startdust.lubogo.activity.category;

import android.app.Activity;

import com.startdust.lubogo.retrofit.model.GetListEstablishmentData;

import java.util.ArrayList;

public class CategoryPresenter implements  CategoryMVP.PresenterMVP{
    CategoryInteractor categoryInteractor;
    CategoryMVP.ViewMVP viewIntance;
    Activity activity;

    public CategoryPresenter(CategoryMVP.ViewMVP viewIntance, Activity activity) {
        this.viewIntance = viewIntance;
        this.activity = activity;

        categoryInteractor = new CategoryInteractor(activity, this);
    }

    @Override
    public void onSuccessListEstablishment(ArrayList<GetListEstablishmentData> getListEstablismentResponse) {
        viewIntance.onSuccessListEstablishment(getListEstablismentResponse);
    }

    @Override
    public void onError(String error) {
        viewIntance.onError(error);
    }

    public void listEstablishment(String id){
        categoryInteractor.listEstablishment(id);
    }
}
