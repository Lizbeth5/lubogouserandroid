package com.startdust.lubogo.retrofit.model;

import java.util.ArrayList;

public class GetListDetailsProductsResponse {
    Boolean response;
    String message;
    String errors;
    ArrayList<GetListDetailsProductsResult> result;

    public GetListDetailsProductsResponse() {
    }

    public GetListDetailsProductsResponse(Boolean response, String message, String errors, ArrayList<GetListDetailsProductsResult> result) {
        this.response = response;
        this.message = message;
        this.errors = errors;
        this.result = result;
    }

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public ArrayList<GetListDetailsProductsResult> getResult() {
        return result;
    }

    public void setResult(ArrayList<GetListDetailsProductsResult> result) {
        this.result = result;
    }
}
