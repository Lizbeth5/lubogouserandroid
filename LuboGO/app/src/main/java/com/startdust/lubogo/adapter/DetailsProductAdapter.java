package com.startdust.lubogo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.startdust.lubogo.R;
import com.startdust.lubogo.retrofit.model.GetListDetailsProductsResult;

import java.util.ArrayList;

public class DetailsProductAdapter extends RecyclerView.Adapter<DetailsProductAdapter.ViewHolder> {
    Context context;
    ArrayList<GetListDetailsProductsResult> data;

    public DetailsProductAdapter(Context context, ArrayList<GetListDetailsProductsResult> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public DetailsProductAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_customizer_order, parent, false);
        return new DetailsProductAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailsProductAdapter.ViewHolder holder, int position) {
        holder.Bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvShowComplement;
        CheckBox cbxSelectOption;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvShowComplement = itemView.findViewById(R.id.tvShowComplement);
            cbxSelectOption = itemView.findViewById(R.id.cbxSelectOption);

        }

        public void Bind(final GetListDetailsProductsResult getListDetailsProductsResult){
            tvShowComplement.setText(getListDetailsProductsResult.getComplemento());

            cbxSelectOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cbxSelectOption.setBackgroundResource(R.drawable.ic_verified_purple);
                }
            });

        }
    }
}
