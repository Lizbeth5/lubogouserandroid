package com.startdust.lubogo.activity.cuztomizerOrder;

import android.app.Activity;

import com.startdust.lubogo.retrofit.RetrofitModule;
import com.startdust.lubogo.retrofit.model.GetInformationProductEstablishmentResponse;
import com.startdust.lubogo.retrofit.model.GetListDetailsProductsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomizerOrderInteractor {
    Activity activity;
    CustomizerOrderMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;
    Call<GetInformationProductEstablishmentResponse> getInformationProductEstablishmentResponseCall;
    Call<GetListDetailsProductsResponse> getListDetailsProductsResponseCall;

    public CustomizerOrderInteractor(Activity activity, CustomizerOrderMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }

    public void informationProduct(String id){
        getInformationProductEstablishmentResponseCall = retrofitModule.getInformationProduct().getInformationProduct(id);
        getInformationProductEstablishmentResponseCall.enqueue(new Callback<GetInformationProductEstablishmentResponse>() {
            @Override
            public void onResponse(Call<GetInformationProductEstablishmentResponse> call, Response<GetInformationProductEstablishmentResponse> response) {
                if (response.isSuccessful()){
                    presenterMVP.onSuccessInformationProduct(response.body().getResult());
                }else {
                    presenterMVP.onError("Error al mostrar la información.");
                }
            }

            @Override
            public void onFailure(Call<GetInformationProductEstablishmentResponse> call, Throwable t) {

            }
        });
    }

    public void listDetailsProduct(String id){
        getListDetailsProductsResponseCall = retrofitModule.getListDetailsProducts().getListDetailsProducts(id);
        getListDetailsProductsResponseCall.enqueue(new Callback<GetListDetailsProductsResponse>() {
            @Override
            public void onResponse(Call<GetListDetailsProductsResponse> call, Response<GetListDetailsProductsResponse> response) {
                if(response.isSuccessful()){
                    presenterMVP.onSuccesListDetailsProduct(response.body().getResult());
                }else{
                    presenterMVP.onError("Error");
                }
            }

            @Override
            public void onFailure(Call<GetListDetailsProductsResponse> call, Throwable t) {

            }
        });
    }

}
