package com.startdust.lubogo.fragment.menu;

import android.app.Activity;

import com.startdust.lubogo.retrofit.RetrofitModule;
import com.startdust.lubogo.retrofit.model.GetListProductEstablishmentResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuTabFragmentInteractor {
    Activity activity;
    MenuTabFragmentMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;
    Call<GetListProductEstablishmentResponse> getListProductEstablishmentResponseCall;

    public MenuTabFragmentInteractor(Activity activity, MenuTabFragmentMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }

    public void listProductsEstablishment(String id){
        getListProductEstablishmentResponseCall = retrofitModule.getListProductsEstablishment().getListProductsEstablishment(id);
        getListProductEstablishmentResponseCall.enqueue(new Callback<GetListProductEstablishmentResponse>() {
            @Override
            public void onResponse(Call<GetListProductEstablishmentResponse> call, Response<GetListProductEstablishmentResponse> response) {
                if(response.isSuccessful()){
                    if (response.body().getResponse()) {
                        presenterMVP.onSuccessListProducts(response.body().getResult());
                    }
                }else{
                    presenterMVP.onError("Error al mostrar Menú");
                }
            }

            @Override
            public void onFailure(Call<GetListProductEstablishmentResponse> call, Throwable t) {

            }
        });
    }
}
