package com.startdust.lubogo.retrofit.model;

public class PutUpdateInformationResponse {
    int result;
    Boolean response;
    String message;
    String errors;

    public PutUpdateInformationResponse() {
    }

    public PutUpdateInformationResponse(int result, Boolean response, String message, String errors) {
        this.result = result;
        this.response = response;
        this.message = message;
        this.errors = errors;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }
}
