package com.startdust.lubogo.retrofit.model;

import java.util.ArrayList;

public class GetListCategoriesResult {
    ArrayList<GetListCategoriesData> Data;

    public GetListCategoriesResult() {
    }

    public GetListCategoriesResult(ArrayList<GetListCategoriesData> data) {
        Data = data;
    }

    public ArrayList<GetListCategoriesData> getData() {
        return Data;
    }

    public void setData(ArrayList<GetListCategoriesData> data) {
        Data = data;
    }
}
