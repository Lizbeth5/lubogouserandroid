package com.startdust.lubogo.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.squareup.picasso.Picasso;
import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.informationUser.InformationUserActivity;
import com.startdust.lubogo.activity.legal.LegalActivity;
import com.startdust.lubogo.activity.login.LoginActivity;
import com.startdust.lubogo.activity.methodPayment.MethodPaymentActivity;
import com.startdust.lubogo.preferences.AppPreferences;
import com.startdust.lubogo.retrofit.model.GetInformationUserResult;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment implements ProfileMVP.ViewMVP{
    TextView tvEditProfile, tvWatchMethodPayment, tvLegal, tvNameUser, tvSignOff, tvWatchFavorites, tvRequestHelpOperator, tvWatchOrders;
    CircleImageView civPhotoUser;
    LinearLayout llSeePromotion;

    private ProfileViewModel profileViewModel;
    ProfilePresenter profilePresenter;
    AppPreferences prefs;

    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        View root = inflater.inflate(R.layout.fragment_profile, container, false);

        tvEditProfile = root.findViewById(R.id.tvEditProfile);
        tvWatchMethodPayment = root.findViewById(R.id.tvWatchMethodPayment);
        tvLegal = root.findViewById(R.id.tvLegal);
        tvNameUser = root.findViewById(R.id.tvNameUser);
        tvSignOff = root.findViewById(R.id.tvSignOff);
        civPhotoUser = root.findViewById(R.id.civPhotoUser);
        tvWatchFavorites = root.findViewById(R.id.tvWatchFavorites);
        tvRequestHelpOperator = root.findViewById(R.id.tvRequestHelpOperator);
        tvWatchOrders = root.findViewById(R.id.tvWatchOrders);
        llSeePromotion = root.findViewById(R.id.llSeePromotion);

        profilePresenter = new ProfilePresenter(getActivity(), this);
        prefs = new AppPreferences(getActivity());


        tvEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), InformationUserActivity.class);
                startActivity(intent);
            }
        });

        tvWatchMethodPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MethodPaymentActivity.class);
                startActivity(intent);
            }
        });

        tvLegal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LegalActivity.class);
                startActivity(intent);
            }
        });

        tvRequestHelpOperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*final Intent intent = new Intent(android.content.Intent.ACTION_SEND);

                intent.setType("text/plain");
                final PackageManager pm = getPackageManager();
                final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
                ResolveInfo best = null;
                for (final ResolveInfo info : matches)
                    if (info.activityInfo.packageName.endsWith(".gm") ||
                            info.activityInfo.name.toLowerCase().contains("gmail")) best = info;
                if (best != null)
                    intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
                startActivity(intent);*/

                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/plain");
                startActivity(emailIntent);
            }
        });

        tvWatchOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Por el momento no esta disponible esta sección.", Toast.LENGTH_SHORT).show();
            }
        });

        tvSignOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefs.setUserId("");
                prefs.setPhoto("");
                prefs.setPhone("");
                prefs.setSexo("");
                prefs.setEmail("");
                prefs.setLastNameMother("");
                prefs.setLastName("");
                prefs.setName("");
                prefs.setUserToken("");
                prefs.setLoginStatus(false);

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });

        tvWatchFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*SavedFragment savedFragment = new SavedFragment();
                FragmentManager fragmentManager = getChildFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.llSaved,savedFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();*/

                Toast.makeText(getContext(), "Error de conexion.", Toast.LENGTH_SHORT).show();
            }
        });

        llSeePromotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Por el momento no esta disponible esta sección.", Toast.LENGTH_SHORT).show();
            }
        });

        profilePresenter.informationUser();
        return root;
    }

    @Override
    public void onSuccessProfile(GetInformationUserResult getInformationUserResult) {
        tvNameUser.setText(getInformationUserResult.getNombre() + " " + getInformationUserResult.getApellidoPaterno());
        Picasso.with(getActivity())
                .load(getInformationUserResult.getUrlFoto())
                .into(civPhotoUser);
        Log.i("nombre usuario",getInformationUserResult.getNombre());
    }

    @Override
    public void onError(String error) {

    }
}
