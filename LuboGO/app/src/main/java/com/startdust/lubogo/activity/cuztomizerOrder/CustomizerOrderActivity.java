package com.startdust.lubogo.activity.cuztomizerOrder;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.startdust.lubogo.R;
import com.startdust.lubogo.adapter.DetailsProductAdapter;
import com.startdust.lubogo.activity.carShopping.CarShoppingActivity;
import com.startdust.lubogo.retrofit.model.GetInformationProductEstablishmentResult;
import com.startdust.lubogo.retrofit.model.GetListDetailsProductsResult;

import java.util.ArrayList;

public class CustomizerOrderActivity extends AppCompatActivity implements CustomizerOrderMVP.ViewMVP{
    ImageView ivAddQuantity, ivRemoveQuantity, ivCloseCustomizeOrder;
    TextView tvQuantityProduct, tvNameProductOrder,tvDescriptionProductOrder;
    RelativeLayout rlAddProduct;

    int cont = 1;
    String idProducto;
    CustomizerOrderPresenter presenter;
    DetailsProductAdapter detailsProductAdapter;
    LinearLayoutManager linearLayoutManager;
    RecyclerView rvListCuztomizerOrder;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customize_order);

        ivAddQuantity = findViewById(R.id.ivAddQuantity);
        ivRemoveQuantity = findViewById(R.id.ivRemoveQuantity);
        tvQuantityProduct = findViewById(R.id.tvQuantityProduct);
        ivCloseCustomizeOrder = findViewById(R.id.ivCloseCustomizeOrder);
        tvNameProductOrder = findViewById(R.id.tvNameProductOrder);
        tvDescriptionProductOrder = findViewById(R.id.tvDescriptionProductOrder);
        rlAddProduct = findViewById(R.id.rlAddProduct);
        rvListCuztomizerOrder = findViewById(R.id.rvListCuztomizerOrder);

        idProducto = getIntent().getExtras().getString("idProducto");
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        presenter = new CustomizerOrderPresenter(this,this);


        ivAddQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cont ++;
                tvQuantityProduct.setText(Integer.toString(cont));
            }
        });

        ivRemoveQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cont <= 0){
                    tvQuantityProduct.setText("0");
                }else{
                    cont--;
                    tvQuantityProduct.setText(Integer.toString(cont));
                }
            }
        });

        ivCloseCustomizeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rlAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomizerOrderActivity.this, CarShoppingActivity.class);
                startActivity(intent);
            }
        });

        presenter.informationProduct(idProducto);
        presenter.listDetailsProducts(idProducto);
    }

    @Override
    public void onSuccessInformationProduct(GetInformationProductEstablishmentResult getInformationProductEstablishmentResult) {
        tvNameProductOrder.setText(getInformationProductEstablishmentResult.getProducto());
        tvDescriptionProductOrder.setText(getInformationProductEstablishmentResult.getDescripcion());

    }

    @Override
    public void onSuccesListDetailsProduct(ArrayList<GetListDetailsProductsResult> getListDetailsProductsResults) {
        detailsProductAdapter = new DetailsProductAdapter(getApplicationContext(),getListDetailsProductsResults);
        rvListCuztomizerOrder.setLayoutManager(linearLayoutManager);
        rvListCuztomizerOrder.setAdapter(detailsProductAdapter);

        rlAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public void onError(String error) {

    }
}
