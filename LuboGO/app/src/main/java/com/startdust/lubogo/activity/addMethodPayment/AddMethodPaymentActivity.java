package com.startdust.lubogo.activity.addMethodPayment;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;

public class AddMethodPaymentActivity extends AppCompatActivity {
    ImageView ivReturnInformationUser;
    TextView tvSaveCard;
    EditText etEnterNameHeadline, etEnterCardNumber, etAddCodeMM,etEnterCodeAA, etEnterCodeCVV;

    @Override
    public void onCreate(Bundle savedIntanceState){
        super.onCreate(savedIntanceState);
        setContentView(R.layout.activity_add_method_payment);

        ivReturnInformationUser = findViewById(R.id.ivReturnInformationUser2);
        tvSaveCard = findViewById(R.id.tvSaveCard);
        etEnterNameHeadline = findViewById(R.id.etEnterNameHeadline);
        etEnterCardNumber = findViewById(R.id.etEnterCardNumber);
        etAddCodeMM = findViewById(R.id.etEnterCodeMM);
        etEnterCodeAA = findViewById(R.id.etEnterCodeAA);
        etEnterCodeCVV = findViewById(R.id.etEnterCodeCVV);

        ivReturnInformationUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
