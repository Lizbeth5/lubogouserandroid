package com.startdust.lubogo.activity.category;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.startdust.lubogo.R;
import com.startdust.lubogo.adapter.EstablishmentAdapter;
import com.startdust.lubogo.retrofit.model.GetListEstablishmentData;

import java.util.ArrayList;

public class CategoryActivity extends AppCompatActivity implements CategoryMVP.ViewMVP{
    ImageView ivCloseCategory, ivSearch;
    TextView tvNameCategory;
    Spinner spOrderBy;
    RecyclerView rvListEstablishment;

    CategoryPresenter presenter;
    EstablishmentAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    String idCategoria;

    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        ivCloseCategory = findViewById(R.id.ivCloseCategory);
        tvNameCategory = findViewById(R.id.tvNameCategory);
        //ivSearch = findViewById(R.id.ivSearch);
        spOrderBy = findViewById(R.id.spOrderBy);
        rvListEstablishment = findViewById(R.id.rvListEstablishment);

        linearLayoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL, false);
        presenter = new CategoryPresenter(this,this);
        idCategoria = getIntent().getExtras().getString("idCategoria");

        ivCloseCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        presenter.listEstablishment(idCategoria);
    }

    @Override
    public void onSuccessListEstablishment(ArrayList<GetListEstablishmentData> getListEstablismentResponse) {
        Log.i("resultado", getListEstablismentResponse.get(0).getEmail());
        adapter = new EstablishmentAdapter( this, getListEstablismentResponse);
        rvListEstablishment.setLayoutManager(linearLayoutManager);
        rvListEstablishment.setAdapter(adapter);
    }

    @Override
    public void onError(String error) {

    }
}


        /*//Para que cuando pulsemos un lugar y este se elimine
        lvListPlaces.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                String idPlace = places.get(position).getId();
                places.remove(position);
                placeNames.remove(position);
                databaseReference.child(PLACE_NODE).child(idPlace).removeValue();
                return true;
            }
        });*/
