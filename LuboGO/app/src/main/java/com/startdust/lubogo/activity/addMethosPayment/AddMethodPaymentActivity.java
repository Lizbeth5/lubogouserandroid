package com.startdust.lubogo.activity.addMethosPayment;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;

public class AddMethodPaymentActivity extends AppCompatActivity {
    ImageView ivReturnInformationUser;

    @Override
    public void onCreate(Bundle savedIntanceState){
        super.onCreate(savedIntanceState);
        setContentView(R.layout.activity_add_method_payment);

        ivReturnInformationUser = findViewById(R.id.ivReturnInformationUser2);

        ivReturnInformationUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
