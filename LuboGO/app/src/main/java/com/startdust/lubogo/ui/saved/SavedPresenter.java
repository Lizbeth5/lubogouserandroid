package com.startdust.lubogo.ui.saved;

import android.app.Activity;

import com.startdust.lubogo.retrofit.model.GetListProductsSavedData;
import com.startdust.lubogo.retrofit.model.PutDeleteProductSavedResponse;

import java.util.ArrayList;

public class SavedPresenter implements SavedMVP.PresenterMVP {
    SavedInteractor savedInteractor;
    SavedMVP.ViewMVP viewMVP;
    Activity activity;


    public SavedPresenter(SavedMVP.ViewMVP viewMVP, Activity activity) {
        this.viewMVP = viewMVP;
        this.activity = activity;

        savedInteractor = new SavedInteractor(activity,this);
    }

    @Override
    public void onSuccessListProductsSaved(ArrayList<GetListProductsSavedData> getListProductsSavedData) {
        viewMVP.onSuccessListProductsSaved(getListProductsSavedData);
    }

    @Override
    public void onSuccessDeleteProductSaved(PutDeleteProductSavedResponse putDeleteProductSavedResponse) {
        viewMVP.onSuccessDeleteProductSaved(putDeleteProductSavedResponse);
    }

    @Override
    public void onError(String error) {
        viewMVP.onError(error);
    }

    public void listProductsSaved(String id){
        savedInteractor.listProductsSaved(id);
    }

    public void deleteProductSaved(String id){
        savedInteractor.deleteProductSaved(id);
    }
}
