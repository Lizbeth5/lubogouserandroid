package com.startdust.lubogo.retrofit.model;

public class GetInformationUserResponse {
    Boolean response;
    String message;
    String errors;
    GetInformationUserResult result;

    public GetInformationUserResponse() {
    }

    public GetInformationUserResponse(Boolean response, String message, String errors, GetInformationUserResult result) {
        this.response = response;
        this.message = message;
        this.errors = errors;
        this.result = result;
    }

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public GetInformationUserResult getResult() {
        return result;
    }

    public void setResult(GetInformationUserResult result) {
        this.result = result;
    }
}
