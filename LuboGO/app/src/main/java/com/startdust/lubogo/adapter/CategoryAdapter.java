package com.startdust.lubogo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.startdust.lubogo.R;
import com.startdust.lubogo.model.CategoryModel;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private Context context;
    private ArrayList<CategoryModel> data;

    public CategoryAdapter(Context context, ArrayList<CategoryModel> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_category, parent, false);
        return new CategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder holder, int position) {
        holder.Bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView ivImageCategory;
        TextView tvNameCategory;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivImageCategory = itemView.findViewById(R.id.ivPhotoCategory);
            tvNameCategory = itemView.findViewById(R.id.tvNameCategory);
        }

        public void Bind(CategoryModel categoryModel) {
            tvNameCategory.setText(categoryModel.getNameCategory());

            String downloadImage = data.get(getPosition()).getUrlImage();
            Glide
                    .with(context)
                    .load(downloadImage)
                    .into(ivImageCategory);

        }
    }
}
