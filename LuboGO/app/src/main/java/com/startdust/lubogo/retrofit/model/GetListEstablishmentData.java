package com.startdust.lubogo.retrofit.model;

public class GetListEstablishmentData {
    String idEstablecimiento;
    String nombre;
    String urlFoto;
    String urlImageLogo;
    String horaApertura;
    String horaCierre;
    String calificacion;
    String informacionEstablecimiento;
    String direccion;
    String password;
    String email;
    String telefono;
    String idTipoEstablecimiento;
    String idStatusEstablecimiento;
    String precioEnvio;

    public GetListEstablishmentData() {
    }

    public GetListEstablishmentData(String idEstablecimiento, String nombre, String urlFoto, String urlImageLogo, String horaApertura, String horaCierre, String calificacion, String informacionEstablecimiento, String direccion, String password, String email, String telefono, String idTipoEstablecimiento, String idStatusEstablecimiento, String precioEnvio) {
        this.idEstablecimiento = idEstablecimiento;
        this.nombre = nombre;
        this.urlFoto = urlFoto;
        this.urlImageLogo = urlImageLogo;
        this.horaApertura = horaApertura;
        this.horaCierre = horaCierre;
        this.calificacion = calificacion;
        this.informacionEstablecimiento = informacionEstablecimiento;
        this.direccion = direccion;
        this.password = password;
        this.email = email;
        this.telefono = telefono;
        this.idTipoEstablecimiento = idTipoEstablecimiento;
        this.idStatusEstablecimiento = idStatusEstablecimiento;
        this.precioEnvio = precioEnvio;
    }

    public String getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public void setIdEstablecimiento(String idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getUrlImageLogo() {
        return urlImageLogo;
    }

    public void setUrlImageLogo(String urlImageLogo) {
        this.urlImageLogo = urlImageLogo;
    }

    public String getHoraApertura() {
        return horaApertura;
    }

    public void setHoraApertura(String horaApertura) {
        this.horaApertura = horaApertura;
    }

    public String getHoraCierre() {
        return horaCierre;
    }

    public void setHoraCierre(String horaCierre) {
        this.horaCierre = horaCierre;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getInformacionEstablecimiento() {
        return informacionEstablecimiento;
    }

    public void setInformacionEstablecimiento(String informacionEstablecimiento) {
        this.informacionEstablecimiento = informacionEstablecimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getIdTipoEstablecimiento() {
        return idTipoEstablecimiento;
    }

    public void setIdTipoEstablecimiento(String idTipoEstablecimiento) {
        this.idTipoEstablecimiento = idTipoEstablecimiento;
    }

    public String getIdStatusEstablecimiento() {
        return idStatusEstablecimiento;
    }

    public void setIdStatusEstablecimiento(String idStatusEstablecimiento) {
        this.idStatusEstablecimiento = idStatusEstablecimiento;
    }

    public String getPrecioEnvio() {
        return precioEnvio;
    }

    public void setPrecioEnvio(String precioEnvio) {
        this.precioEnvio = precioEnvio;
    }
}
