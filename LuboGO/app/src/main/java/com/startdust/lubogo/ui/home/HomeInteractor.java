package com.startdust.lubogo.ui.home;

import android.app.Activity;

import com.startdust.lubogo.retrofit.RetrofitModule;
import com.startdust.lubogo.retrofit.model.GetListCategoriesResponse;
import com.startdust.lubogo.retrofit.model.GetListOffertResponse;
import com.startdust.lubogo.retrofit.model.PostSavedProductResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeInteractor {
    Activity activity;
    HomeMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;
    Call<GetListOffertResponse> getListOffertResponseCall;
    Call<GetListCategoriesResponse> getListCategoriesResponseCall;
    Call<PostSavedProductResponse> postSavedProductResponseCall;

    public HomeInteractor(Activity activity, HomeMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }

    public void listOfferts(){
        getListOffertResponseCall = retrofitModule.getListOfferts().getListOfferts();
        getListOffertResponseCall.enqueue(new Callback<GetListOffertResponse>() {
            @Override
            public void onResponse(Call<GetListOffertResponse> call, Response<GetListOffertResponse> response) {
                if(response.isSuccessful()){
                    presenterMVP.onSuccesListOffert(response.body().getResult());
                }
            }

            @Override
            public void onFailure(Call<GetListOffertResponse> call, Throwable t) {

            }
        });
    }

    public void listCategories(){
        getListCategoriesResponseCall = retrofitModule.getListCategories().getListCategories();
        getListCategoriesResponseCall.enqueue(new Callback<GetListCategoriesResponse>() {
            @Override
            public void onResponse(Call<GetListCategoriesResponse> call, Response<GetListCategoriesResponse> response) {
                if (response.isSuccessful()){
                    if(response.body().getResponse()) {
                        presenterMVP.onSuccesListCategories(response.body().getResult().getData());
                    }
                }else{
                    presenterMVP.onError("Error al mostrar categorias");
                }

            }

            @Override
            public void onFailure(Call<GetListCategoriesResponse> call, Throwable t) {

            }
        });
    }

    public void savedProduct(String idUser, String idProduct, String idStatusSaved){
        postSavedProductResponseCall = retrofitModule.postSavedProduct().postSavedProduct(idUser, idProduct, idStatusSaved);
        postSavedProductResponseCall.enqueue(new Callback<PostSavedProductResponse>() {
            @Override
            public void onResponse(Call<PostSavedProductResponse> call, Response<PostSavedProductResponse> response) {
                if (response.isSuccessful()){
                    presenterMVP.onSuccesSavedProduct(response.body());
                }else{
                    presenterMVP.onError("Error");
                }
            }

            @Override
            public void onFailure(Call<PostSavedProductResponse> call, Throwable t) {

            }
        });
    }

}
