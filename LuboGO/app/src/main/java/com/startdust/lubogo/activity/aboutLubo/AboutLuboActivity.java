package com.startdust.lubogo.activity.aboutLubo;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;

public class AboutLuboActivity extends AppCompatActivity {
    ImageView ivReturnLegal;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_lubo);

        ivReturnLegal = findViewById(R.id.ivReturnLegal2);

        ivReturnLegal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
