package com.startdust.lubogo.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.profileEstablishment.ProfileEstablishmentActivity;
import com.startdust.lubogo.retrofit.model.GetListEstablishmentData;

import java.util.ArrayList;

public class EstablishmentAdapter extends RecyclerView.Adapter<EstablishmentAdapter.ViewHolder> {
    Context context;
    ArrayList<GetListEstablishmentData> data;

    public EstablishmentAdapter(Context context, ArrayList<GetListEstablishmentData> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public EstablishmentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_establishment, parent, false);
        return new EstablishmentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.Bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView ivSeePhotoEstablishent;
        TextView tvSeeNameEstablishment, tvSeeTypeEstablishment,tvSeeQualificationEstablishment, tvSeeTimeDear,
                tvSeeTextPedido, tvSeeShippingCost;
        RelativeLayout rlProfileEstablishment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivSeePhotoEstablishent = itemView.findViewById(R.id.ivSeePhotoEstablishent);
            tvSeeNameEstablishment = itemView.findViewById(R.id.tvSeeNameEstablishment);
            tvSeeTypeEstablishment = itemView.findViewById(R.id.tvSeeTypeEstablishment);
            tvSeeQualificationEstablishment = itemView.findViewById(R.id.tvSeeQualificationEstablishment);
            tvSeeTimeDear = itemView.findViewById(R.id.tvSeeTimeDear);
            tvSeeTextPedido = itemView.findViewById(R.id.tvSeeTextPedido);
            tvSeeShippingCost = itemView.findViewById(R.id.tvSeeShippingCost);
            rlProfileEstablishment = itemView.findViewById(R.id.rlEstablishment);
        }

        public void Bind(final GetListEstablishmentData getListEstablishmentData){
            Log.i("datos", getListEstablishmentData.getDireccion());
            tvSeeNameEstablishment.setText(getListEstablishmentData.getNombre());
            tvSeeTypeEstablishment.setText("Restaurante");//getListEstablishmentData.getIdTipoEstablecimiento());
            tvSeeQualificationEstablishment.setText(getListEstablishmentData.getCalificacion());
            tvSeeShippingCost.setText("MXN" + getListEstablishmentData.getPrecioEnvio());

            String downloadImage = data.get(getPosition()).getUrlFoto();
            Glide
                    .with(context)
                    .load(downloadImage)
                    .into(ivSeePhotoEstablishent);

            rlProfileEstablishment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProfileEstablishmentActivity.class);
                    intent.putExtra("idEstablecimiento", getListEstablishmentData.getIdEstablecimiento());
                    context.startActivity(intent);
                }
            });
        }

    }
}
