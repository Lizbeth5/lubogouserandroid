package com.startdust.lubogo.activity.profileEstablishment;

import android.app.Activity;
import android.util.Log;

import com.startdust.lubogo.retrofit.RetrofitModule;
import com.startdust.lubogo.retrofit.model.GetInformationProfileEstablishmentResponse;
import com.startdust.lubogo.retrofit.model.GetListProductEstablishmentResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileEstablishmentInteractor {
    Activity activity;
    ProfileEstablishmentMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;
    Call<GetInformationProfileEstablishmentResponse> getInformationProfileEstablishmentResponseCall;
    Call<GetListProductEstablishmentResponse> getListProductEstablishmentResponseCall;

    public ProfileEstablishmentInteractor(Activity activity, ProfileEstablishmentMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }

    public void informationEstablishment(String id){
        getInformationProfileEstablishmentResponseCall = retrofitModule.getInformationProfileEstablishment().getInformationProfileEstablishment(id);
        getInformationProfileEstablishmentResponseCall.enqueue(new Callback<GetInformationProfileEstablishmentResponse>() {
            @Override
            public void onResponse(Call<GetInformationProfileEstablishmentResponse> call, Response<GetInformationProfileEstablishmentResponse> response) {
                if(response.isSuccessful()){
                    presenterMVP.onSuccessInformationEstablishment(response.body().getResult());
                }else {
                    presenterMVP.onError("Error al mostrar la información");
                }
            }

            @Override
            public void onFailure(Call<GetInformationProfileEstablishmentResponse> call, Throwable t) {
                Log.i("perfil esta", t.getMessage());
            }
        });
    }

    public void listProductEstablishment(String id){
        getListProductEstablishmentResponseCall = retrofitModule.getListProductsEstablishment().getListProductsEstablishment(id);
        getListProductEstablishmentResponseCall.enqueue(new Callback<GetListProductEstablishmentResponse>() {
            @Override
            public void onResponse(Call<GetListProductEstablishmentResponse> call, Response<GetListProductEstablishmentResponse> response) {
                if(response.isSuccessful()){
                    presenterMVP.onSuccessListProductsEstablishment(response.body().getResult());
                }else{
                    presenterMVP.onError("Error al mostrar la información");
                }
            }

            @Override
            public void onFailure(Call<GetListProductEstablishmentResponse> call, Throwable t) {

            }
        });
    }

}
