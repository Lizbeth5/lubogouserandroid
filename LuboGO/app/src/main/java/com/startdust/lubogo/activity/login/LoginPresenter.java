package com.startdust.lubogo.activity.login;

import android.app.Activity;

import com.startdust.lubogo.model.User;

public class LoginPresenter implements LoginMVP.PresenterMVP {
    LoginMVP.ViewMVP loginMVP;
    LoginInteractor loginInteractor;
    Activity activity;

    public LoginPresenter( LoginMVP.ViewMVP loginMVP, Activity activity) {
        this.loginMVP = loginMVP;
        this.activity = activity;

        loginInteractor = new LoginInteractor(activity,this);
    }

    @Override
    public void onSuccesssLogin() {
        loginMVP.onSuccessLogin();
    }

    @Override
    public void onError(String error) {
        loginMVP.onError(error);
    }

    public void login(final User user){
        if(user != null){
            if(user.getEmail().isEmpty() || user.getPassword().isEmpty()){
                loginMVP.onError("Llenar todos los campos");
            }else{
                loginInteractor.postLogin(user);
            }
        }
    }


}
