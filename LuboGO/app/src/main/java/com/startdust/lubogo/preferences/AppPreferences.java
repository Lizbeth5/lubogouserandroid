package com.startdust.lubogo.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {
    private SharedPreferences appData;
    private SharedPreferences.Editor appEdit;

    public AppPreferences (Context context){
        appData = context.getSharedPreferences("ShareData", Context.MODE_PRIVATE); //manera de guardar información
    }
    //para verificacr si se inicio sesion o no (Boolean)
    public boolean isLogued(){
        return appData.getBoolean("loginStatus",false);
    }

    public void setLoginStatus(boolean status){
        appEdit = appData.edit();
        appEdit.putBoolean("loginStatus", status);
        appEdit.apply();
    }

    public String getUserId(){
        return appData.getString("userId","");
    }

    public void setUserId(String id){
        appEdit = appData.edit();
        appEdit.putString("userId", id);
        appEdit.apply();
    }


    public void setUserToken(String token){
        appEdit = appData.edit();
        appEdit.putString("userToken",token);
        appEdit.apply();
    }

    public String  getName(){
        return appData.getString("name", "");
    }

    public void setName(String name){
        appEdit = appData.edit();
        appEdit.putString("firstName", name);
        appEdit.apply();
    }

    public String getLastName(){
        return appData.getString("lastName", "");
    }

    public void setLastName(String lastName){
        appEdit = appData.edit();
        appEdit.putString("lastName", lastName);
        appEdit.apply();
    }

    public String getLastNameMother(){
        return appData.getString("lastNameMother", "");
    }

    public void setLastNameMother(String lastNameMother){
        appEdit = appData.edit();
        appEdit.putString("lastNameMother", lastNameMother);
        appEdit.apply();
    }

    public String getEmail(){
        return appData.getString("email", "");
    }

    public void setEmail(String email){
        appEdit = appData.edit();
        appEdit.putString("email", email);
        appEdit.apply();
    }

    public String getSexo(){
        return appData.getString("sexo", "");
    }

    public void setSexo(String sexo){
        appEdit = appData.edit();
        appEdit.putString("sexo", sexo);
        appEdit.apply();
    }

    public String getPhoto(){
        return appData.getString("photo", "");
    }

    public void setPhoto(String photo){
        appEdit = appData.edit();
        appEdit.putString("photo", photo);
        appEdit.apply();
    }

    public String getPhone(){
        return appData.getString("phone", "");
    }

    public void setPhone(String phone){
        appEdit = appData.edit();
        appEdit.putString("phone", phone);
        appEdit.apply();
    }

}
