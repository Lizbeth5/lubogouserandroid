package com.startdust.lubogo.activity.code;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;

public class CodeActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code);
    }
}
