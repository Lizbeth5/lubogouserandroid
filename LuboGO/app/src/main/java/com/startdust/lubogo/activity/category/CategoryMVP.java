package com.startdust.lubogo.activity.category;

import com.startdust.lubogo.retrofit.model.GetListEstablishmentData;
import com.startdust.lubogo.retrofit.model.GetListEstablishmentDataResult;
import com.startdust.lubogo.retrofit.model.GetListEstablismentResponse;

import java.util.ArrayList;

public interface CategoryMVP {

    interface ViewMVP{
        void onSuccessListEstablishment(ArrayList<GetListEstablishmentData> getListEstablismentResponse);
        void onError(String error);
    }

    interface PresenterMVP{
        void onSuccessListEstablishment(ArrayList<GetListEstablishmentData> getListEstablismentResponse);
        void onError(String error);
    }
}
