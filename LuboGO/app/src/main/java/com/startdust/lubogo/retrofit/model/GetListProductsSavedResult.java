package com.startdust.lubogo.retrofit.model;

import java.util.ArrayList;

public class GetListProductsSavedResult {
    ArrayList<GetListProductsSavedData> Data;

    public GetListProductsSavedResult() {
    }

    public GetListProductsSavedResult(ArrayList<GetListProductsSavedData> data) {
        Data = data;
    }

    public ArrayList<GetListProductsSavedData> getData() {
        return Data;
    }

    public void setData(ArrayList<GetListProductsSavedData> data) {
        Data = data;
    }
}
