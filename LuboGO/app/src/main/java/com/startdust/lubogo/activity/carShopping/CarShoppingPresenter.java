package com.startdust.lubogo.activity.carShopping;

import android.app.Activity;

public class CarShoppingPresenter implements CarShoppingMVP.PresenterMVP {
    Activity activity;
    CarShoppingMVP.ViewMVP viewMVP;
    CarShoppingInteractor carShoppingInteractor;

    public CarShoppingPresenter(Activity activity, CarShoppingMVP.ViewMVP viewMVP) {
        this.activity = activity;
        this.viewMVP = viewMVP;

        carShoppingInteractor = new CarShoppingInteractor(activity, this);
    }
}
