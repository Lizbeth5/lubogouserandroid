package com.startdust.lubogo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.profileEstablishment.ProfileEstablishmentActivity;
import com.startdust.lubogo.retrofit.model.GetListProductsSavedData;
import com.startdust.lubogo.ui.saved.SavedFragment;

import java.util.ArrayList;

public class SavedAdapter extends RecyclerView.Adapter<SavedAdapter.ViewHolder> {
    Context context;
    ArrayList<GetListProductsSavedData> data;

    public SavedAdapter(Context context, ArrayList<GetListProductsSavedData> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public SavedAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_saved, parent, false);
        return new SavedAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.Bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivPhotoEstablishentSave, ivProductSaved;
        TextView tvNameEstablishmentSave, tvTypeEstablishmentSave, tvNameProductSave, tvShippingCostSave;
        CardView cvProductSaved;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPhotoEstablishentSave = itemView.findViewById(R.id.ivPhotoEstablishentSave);
            tvNameEstablishmentSave = itemView.findViewById(R.id.tvNameEstablishmentSave);
            tvTypeEstablishmentSave = itemView.findViewById(R.id.tvTypeEstablishmentSave);
            tvNameProductSave = itemView.findViewById(R.id.tvNameProductSave);
            tvShippingCostSave = itemView.findViewById(R.id.tvShippingCostSave);
            cvProductSaved = itemView.findViewById(R.id.cvProductSaved);
            ivProductSaved = itemView.findViewById(R.id.ivProductSaved);

        }

        public void Bind(final GetListProductsSavedData getListProductsSavedData){
            tvNameEstablishmentSave.setText(getListProductsSavedData.getNombre());
            tvTypeEstablishmentSave.setText(getListProductsSavedData.getDescripcion());
            tvNameProductSave.setText(getListProductsSavedData.getProducto());
            tvShippingCostSave.setText("$ " + getListProductsSavedData.getPrecio());

            String downloadImage = getListProductsSavedData.getUrlFoto();
            Picasso.with(context)
                    .load(downloadImage)
                    .into(ivPhotoEstablishentSave);

            cvProductSaved.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProfileEstablishmentActivity.class);
                    intent.putExtra("idEstablecimiento", getListProductsSavedData.getIdEstablecimiento());
                    context.startActivity(intent);
                }
            });

            ivProductSaved.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //deleteProductSaved("idProducto" + getListProductsSavedData.getIdProducto());
                    Intent intent = new Intent(context, SavedFragment.class);
                    intent.putExtra("idProducto", getListProductsSavedData.getIdProducto());
                    context.startService(intent);
                }
            });
        }
    }
}
