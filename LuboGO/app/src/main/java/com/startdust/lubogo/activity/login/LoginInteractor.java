package com.startdust.lubogo.activity.login;

import android.app.Activity;
import android.util.Log;

import com.startdust.lubogo.model.User;
import com.startdust.lubogo.preferences.AppPreferences;
import com.startdust.lubogo.retrofit.RetrofitModule;
import com.startdust.lubogo.retrofit.model.GetDataResponse;
import com.startdust.lubogo.retrofit.model.PostLoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginInteractor  {
    Activity activity;
    LoginMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;
    Call<PostLoginResponse> postLoginResponseCall;
    Call<GetDataResponse> getDataResponseCall;
    AppPreferences prefs;

    public LoginInteractor(Activity activity, LoginMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
        prefs = new AppPreferences(activity);
    }

    //consumir el servicio
    public void postLogin(final User user){
        postLoginResponseCall = retrofitModule.postLogin().postLogin(user.getEmail(), user.getPassword(),"1");
        postLoginResponseCall.enqueue(new Callback<PostLoginResponse>() {
            @Override
            public void onResponse(Call<PostLoginResponse> call, Response<PostLoginResponse> response) {
                PostLoginResponse postLoginResponse = response.body();
                if(postLoginResponse.getResponse()){
                    presenterMVP.onSuccesssLogin();
                    getData(response.body().getResult().getToken());
                    prefs.setLoginStatus(true); //status verdadero
                    prefs.setUserId(response.body().getResult().getId());//para guardar el id

                }else{
                    presenterMVP.onError("Error, verifique sus datos.");

                }
            }

            //Si ocurre un error lo imprime en el log
            @Override
            public void onFailure(Call<PostLoginResponse> call, Throwable t) {
                Log.i("Error Iniciar Sesion", t.getMessage());
            }
        });
    }

    public void getData(final String token){
        getDataResponseCall = retrofitModule.getData().getData(token);
        getDataResponseCall.enqueue(new Callback<GetDataResponse>() {
            @Override
            public void onResponse(Call<GetDataResponse> call, Response<GetDataResponse> response) {
                GetDataResponse getDataResponse = response.body();
                if(getDataResponse.getResponse()){
                    prefs.setUserId(response.body().getResult().getIdUsuario());
                    prefs.setName(response.body().getResult().getNombre());
                    prefs.setLastName(response.body().getResult().getApellidoPaterno());
                    prefs.setLastNameMother(response.body().getResult().getApellidoMaterno());
                    prefs.setEmail(response.body().getResult().getEmail());
                    prefs.setPhone(response.body().getResult().getTelefono());
                    prefs.setSexo(response.body().getResult().getSexo());
                    prefs.setPhoto(response.body().getResult().getUrlFoto());
                }
            }

            @Override
            public void onFailure(Call<GetDataResponse> call, Throwable t) {
                Log.i("Error token", t.getMessage());
            }
        });
    }
}
