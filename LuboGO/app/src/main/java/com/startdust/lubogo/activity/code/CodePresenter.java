package com.startdust.lubogo.activity.code;

import android.app.Activity;

public class CodePresenter implements CodeMVP.PresenterMVP {

    CodeInteractor codeInteractor;
    CodeMVP.ViewMVP viewIntance;
    Activity activity;

    public CodePresenter(CodeMVP.ViewMVP viewIntance, Activity activity) {
        this.viewIntance = viewIntance;
        this.activity = activity;

        codeInteractor = new CodeInteractor(activity, this);
    }
}
