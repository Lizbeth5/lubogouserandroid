package com.startdust.lubogo.retrofit.model;

public class GetInformationProfileEstablishmentResult {
    String idEstablecimiento;
    String nombre;
    String urlFoto;
    String urlImageLogo;
    String calificacion;
    String informacionEstablecimiento;
    String direccion;
    String password;
    String email;
    String telefono;
    String idTipoEstablecimiento;
    String idStatusEstablecimiento;
    Double precioEnvio;

    public GetInformationProfileEstablishmentResult() {
    }

    public GetInformationProfileEstablishmentResult(String idEstablecimiento, String nombre, String urlFoto, String urlImageLogo, String calificacion, String informacionEstablecimiento, String direccion, String password, String email, String telefono, String idTipoEstablecimiento, String idStatusEstablecimiento, Double precioEnvio) {
        this.idEstablecimiento = idEstablecimiento;
        this.nombre = nombre;
        this.urlFoto = urlFoto;
        this.urlImageLogo = urlImageLogo;
        this.calificacion = calificacion;
        this.informacionEstablecimiento = informacionEstablecimiento;
        this.direccion = direccion;
        this.password = password;
        this.email = email;
        this.telefono = telefono;
        this.idTipoEstablecimiento = idTipoEstablecimiento;
        this.idStatusEstablecimiento = idStatusEstablecimiento;
        this.precioEnvio = precioEnvio;
    }

    public String getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public void setIdEstablecimiento(String idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getUrlImageLogo() {
        return urlImageLogo;
    }

    public void setUrlImageLogo(String urlImageLogo) {
        this.urlImageLogo = urlImageLogo;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getInformacionEstablecimiento() {
        return informacionEstablecimiento;
    }

    public void setInformacionEstablecimiento(String informacionEstablecimiento) {
        this.informacionEstablecimiento = informacionEstablecimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getIdTipoEstablecimiento() {
        return idTipoEstablecimiento;
    }

    public void setIdTipoEstablecimiento(String idTipoEstablecimiento) {
        this.idTipoEstablecimiento = idTipoEstablecimiento;
    }

    public String getIdStatusEstablecimiento() {
        return idStatusEstablecimiento;
    }

    public void setIdStatusEstablecimiento(String idStatusEstablecimiento) {
        this.idStatusEstablecimiento = idStatusEstablecimiento;
    }

    public Double getPrecioEnvio() {
        return precioEnvio;
    }

    public void setPrecioEnvio(Double precioEnvio) {
        this.precioEnvio = precioEnvio;
    }
}
