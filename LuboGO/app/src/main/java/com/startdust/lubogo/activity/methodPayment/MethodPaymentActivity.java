package com.startdust.lubogo.activity.methodPayment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.addMethodPayment.AddMethodPaymentActivity;

public class MethodPaymentActivity extends AppCompatActivity {
    ImageView ivCloseMethodPayment, ivAddCard;

    @Override
    public void onCreate(Bundle savedIntanceState){
        super.onCreate(savedIntanceState);
        setContentView(R.layout.activity_method_payment);

        ivCloseMethodPayment = findViewById(R.id.ivCloseMethodPayment);
        ivAddCard = findViewById(R.id.ivAddCard);

        ivCloseMethodPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivAddCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MethodPaymentActivity.this, AddMethodPaymentActivity.class);
                startActivity(intent);
            }
        });
    }
}
