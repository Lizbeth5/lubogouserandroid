package com.startdust.lubogo.activity.legal;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.aboutLubo.AboutLuboActivity;
import com.startdust.lubogo.activity.privacyPolicy.PrivacyPolicyActivity;
import com.startdust.lubogo.activity.termsAndConditions.TermsAndConditionsActivity;

public class LegalActivity extends AppCompatActivity {
    ImageView ivReturnInformationUser;
    RelativeLayout rlShowTermsAndConditions, rlShowPrivacyPolicy, rlShowAboutFromLubo;

    @Override
    public void onCreate(Bundle savedIntanceState){
        super.onCreate(savedIntanceState);
        setContentView(R.layout.activity_legal);

        ivReturnInformationUser = findViewById(R.id.ivReturnInformationUser3);
        rlShowTermsAndConditions = findViewById(R.id.rlShowTermsAndConditions);
        rlShowPrivacyPolicy = findViewById(R.id.rlShowPrivacyPolicy);
        rlShowAboutFromLubo = findViewById(R.id.rlShowAboutFromLubo);

        ivReturnInformationUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rlShowTermsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LegalActivity.this, TermsAndConditionsActivity.class);
                startActivity(intent);
            }
        });

        rlShowPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LegalActivity.this, PrivacyPolicyActivity.class);
                startActivity(intent);
            }
        });

        rlShowAboutFromLubo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LegalActivity.this, AboutLuboActivity.class);
                startActivity(intent);
            }
        });
    }
}
