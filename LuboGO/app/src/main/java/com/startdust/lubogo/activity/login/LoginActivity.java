package com.startdust.lubogo.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.home.MainActivity;
import com.startdust.lubogo.model.User;
import com.startdust.lubogo.activity.recoverPassword.RecoverPasswordActivity;
import com.startdust.lubogo.activity.registry.RegistryActivity;

public class LoginActivity extends AppCompatActivity implements LoginMVP.ViewMVP{
    EditText etEmail, etPassword;
    Button btnLogin;
    ImageView ivEyePassword;
    TextView tvRecoverPassword, tvRegistry;

    LoginPresenter loginPresenter;

    @Override
    public void onCreate(Bundle savedIntanceState){
        super.onCreate(savedIntanceState);
        setContentView(R.layout.activity_login);

        etEmail = findViewById(R.id.etEnterOkyEmailLogin);
        etPassword = findViewById(R.id.etEnterOkPasswordLogin);
        btnLogin = findViewById(R.id.btnLogin);
        ivEyePassword = findViewById(R.id.ivEyePassword);
        tvRecoverPassword = findViewById(R.id.tvRecoverPassword);
        tvRegistry = findViewById(R.id.tvRegistry);

        loginPresenter = new LoginPresenter(this,this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User();
                user.setEmail(etEmail.getText().toString());
                user.setPassword(etPassword.getText().toString());
                loginPresenter.login(user);
            }
        });

        ivEyePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_NORMAL);
                ivEyePassword.setImageResource(R.drawable.ic_eye_close);
            }
        });

        tvRecoverPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RecoverPasswordActivity.class);
                startActivity(intent);
            }
        });
        tvRegistry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistryActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onSuccessLogin() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();

    }
}
