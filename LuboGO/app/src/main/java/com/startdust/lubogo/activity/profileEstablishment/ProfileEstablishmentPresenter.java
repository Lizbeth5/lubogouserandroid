package com.startdust.lubogo.activity.profileEstablishment;

import android.app.Activity;
import android.util.Log;

import com.startdust.lubogo.retrofit.model.GetInformationProfileEstablishmentResult;
import com.startdust.lubogo.retrofit.model.GetListProductEstablishmentResult;

import java.util.ArrayList;

public class ProfileEstablishmentPresenter implements ProfileEstablishmentMVP.PresenterMVP{
    ProfileEstablishmentInteractor profileEstablishmentInteractor;
    ProfileEstablishmentMVP.ViewMVP viewMVP;
    Activity activity;

    public ProfileEstablishmentPresenter(ProfileEstablishmentMVP.ViewMVP viewMVP, Activity activity) {
        this.viewMVP = viewMVP;
        this.activity = activity;

        profileEstablishmentInteractor = new ProfileEstablishmentInteractor(activity,this);
    }

    @Override
    public void onSuccessInformationEstablishment(GetInformationProfileEstablishmentResult getInformationProfileEstablishmentResult) {
        viewMVP.onSuccessInformationEstablishment(getInformationProfileEstablishmentResult);
    }

    @Override
    public void onSuccessListProductsEstablishment(ArrayList<GetListProductEstablishmentResult> getListProductEstablishmentResults) {
        viewMVP.onSuccessListProductsEstablishment(getListProductEstablishmentResults);
    }

    @Override
    public void onError(String error) {
        viewMVP.onError(error);
    }

    public void informationEstablishment(String id){
        profileEstablishmentInteractor.informationEstablishment(id);
        Log.i("id", id);
    }

    public void listProductsEstablishment(String id){
        profileEstablishmentInteractor.informationEstablishment(id);
    }
}
