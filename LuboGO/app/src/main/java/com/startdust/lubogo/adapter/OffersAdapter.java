package com.startdust.lubogo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.startdust.lubogo.R;
import com.startdust.lubogo.model.OffertsModel;

import java.util.ArrayList;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.ViewHolder>{
    private Context context;
    private ArrayList<OffertsModel> data;

    public OffersAdapter(Context context, ArrayList<OffertsModel> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_ofertas, parent, false);
        return new OffersAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.Bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView ivPhotoEstablishentItem, ivSaveItem;
        TextView tvNameEstablishmentItem, tvTypeEstablishmentItem, tvQualificationEstablishmentItem, tvTimeDearItem, tvShippingCostItem, tvTextPedido;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPhotoEstablishentItem = itemView.findViewById(R.id.ivPhotoEstablishentOffers);
            tvNameEstablishmentItem = itemView.findViewById(R.id.tvNameEstablishmentOffers);
         //   tvTypeEstablishmentItem = itemView.findViewById(R.id.tvTypeEstablishmentItem);
            tvQualificationEstablishmentItem = itemView.findViewById(R.id.tvQualificationEstablishmentOffers);
            tvTimeDearItem = itemView.findViewById(R.id.tvTimeDearOffers);
            tvShippingCostItem = itemView.findViewById(R.id.tvShippingCostOffers);
            tvTextPedido = itemView.findViewById(R.id.tvTextPedidoOffers);
        }

        public void Bind(OffertsModel offertsModel) {
            tvNameEstablishmentItem.setText(offertsModel.getNameEstablishment());
            tvQualificationEstablishmentItem.setText("" + offertsModel.getQualification());
           // tvTypeEstablishmentItem.setText(offertsModel.getTypeEstablishment());
            tvTimeDearItem.setText(offertsModel.getTime());
            tvShippingCostItem.setText(offertsModel.getCost());
            tvTextPedido.setText(""+offertsModel.getOrders());

            /*RequestOptions requestOptions = new RequestOptions();
            requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));

            Glide.with(context)
                    .load(data.get(getAdapterPosition()).getUrlImage())
                    .apply(requestOptions)
                    .into(ivPhotoEstablishentItem);*/

            /*String downloadImage = data.get(position).getUrlImage(); //para que la url no sea nula

            Glide.with(context)
                    .load(downloadImage) //cargar el link de donde esta almacenada la foto
                    .fitCenter() //para acomodar la foto
                    .centerCrop() //para reajustar la imagen dentro del ImageView
                    .error(R.drawable.ic_image_black_) //por si no encuentra nada muestra la imagen asignada
                    .into(ivPhotoEstablishentItem); //en donde queremos que se muestre la imagen*/

            //return rowView;

        }
    }
}
