package com.startdust.lubogo.retrofit;

import com.startdust.lubogo.retrofit.model.GetDataResponse;
import com.startdust.lubogo.retrofit.model.GetInformationProductEstablishmentResponse;
import com.startdust.lubogo.retrofit.model.GetInformationProfileEstablishmentResponse;
import com.startdust.lubogo.retrofit.model.GetInformationUserResponse;
import com.startdust.lubogo.retrofit.model.GetListCategoriesResponse;
import com.startdust.lubogo.retrofit.model.GetListDetailsProductsResponse;
import com.startdust.lubogo.retrofit.model.GetListEstablismentResponse;
import com.startdust.lubogo.retrofit.model.GetListOffertResponse;
import com.startdust.lubogo.retrofit.model.GetListProductEstablishmentResponse;
import com.startdust.lubogo.retrofit.model.GetListProductsSavedResponse;
import com.startdust.lubogo.retrofit.model.GetListScheduleServiceEstablishmentResponse;
import com.startdust.lubogo.retrofit.model.PostLoginResponse;
import com.startdust.lubogo.retrofit.model.PostSavedProductResponse;
import com.startdust.lubogo.retrofit.model.PutDeleteProductSavedResponse;
import com.startdust.lubogo.retrofit.model.PutUpdateInformationResponse;
import com.startdust.lubogo.retrofit.model.SetRegistryResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RetrofitAPI {
    interface SetRegistry{
        @POST("user/registerUser")
        @FormUrlEncoded
        Call<SetRegistryResponse> setRegistry (@Field("nombre") String name,
                                               @Field("apellidoMaterno") String lastNameMother,
                                               @Field("apellidoPaterno") String lastName,
                                               @Field("email") String email,
                                               @Field("password") String password,
                                               @Field("idTipoUsuario") String idTypeUser);

    }

    interface PostLogin{
        @POST("auth/loginUser")
        @FormUrlEncoded
        Call<PostLoginResponse> postLogin (@Field("email") String email,
                                           @Field("password") String password,
                                           @Field("idTipoUsuario") String idTypeUser);
    }

    interface GetData{
        @GET("auth/getData/{token}")
        Call<GetDataResponse> getData(@Path("token") String token);
    }

    interface GetInformationUser{
        @GET("user/informationUser/{id}")
        Call<GetInformationUserResponse>    getInformationUser(@Path("id") String id);
    }

    interface PutUpdateInformation{
        @PUT("user/updateInformationUser/{id}")
        @FormUrlEncoded
        Call<PutUpdateInformationResponse> putUpdateInformation(@Path("id") String id,
                                                                @Field("nombre") String name,
                                                                @Field("apellidoPaterno") String lastName);
    }

    interface GetListEstablishment{
        @GET("establishment/toListEstablishment/{idTipoEstablecimiento}")
        Call<GetListEstablismentResponse> getListEstablishment(@Path("idTipoEstablecimiento") String id);
    }

    interface GetInformationProfileEstablishment{
        @GET("establishment/informationProfile/{idEstablecimiento}")
        Call<GetInformationProfileEstablishmentResponse> getInformationProfileEstablishment(@Path("idEstablecimiento") String id);
    }

    interface GetListOfferts{
        @GET("offert/obtainOfferts")
        Call<GetListOffertResponse> getListOfferts();
    }

    interface GetListCategories{
        @GET("establishment/listCategories")
        Call<GetListCategoriesResponse> getListCategories();
    }

    interface GetListProductsEstablishment{
        @GET("product/toListProductsEstablishment/{idEstablecimiento}")
        Call<GetListProductEstablishmentResponse> getListProductsEstablishment(@Path("idEstablecimiento") String id);
    }

    interface GetInformationProduct{
        @GET("product/informationProduct/{idProducto}")
        Call<GetInformationProductEstablishmentResponse> getInformationProduct(@Path("idProducto") String id);
    }

    interface GetListScheduleServiceEstablishment{
        @GET("schedule/listSchedule/{idEstablecimiento}")
        Call<GetListScheduleServiceEstablishmentResponse> getListScheduleServiceEstablishment(@Path("idEstablecimiento") String id);
    }

    interface GetListProductsSaved{
        @GET("guarded/listSaved/{idUsuario}")
        Call<GetListProductsSavedResponse> getListProductsSaved(@Path("idUsuario") String id);
    }

    interface GetListDetailsProducts{
        @GET("product/listDetailsProducts/{idProducto}")
        Call<GetListDetailsProductsResponse> getListDetailsProducts(@Path("idProducto") String id);
    }

    interface PostSavedProduct{
        @POST("guarded/guarded")
        @FormUrlEncoded
        Call<PostSavedProductResponse> postSavedProduct(@Field("idUsuario") String idUser,
                                                        @Field("idProducto") String idProduct,
                                                        @Field("idStatusGuardado") String idStatusSaved);
    }

    interface PutDeleteProductSaved{
        @PUT("guarded/deleteElementSaved/{idProducto}")
        @FormUrlEncoded
        Call<PutDeleteProductSavedResponse> putDeleteProductSaved(@Field("idProducto") String idProduct);
    }
}
