package com.startdust.lubogo.ui.home;

import com.startdust.lubogo.retrofit.model.GetListCategoriesData;
import com.startdust.lubogo.retrofit.model.GetListCategoriesResponse;
import com.startdust.lubogo.retrofit.model.GetListOffertResult;
import com.startdust.lubogo.retrofit.model.PostSavedProductResponse;

import java.util.ArrayList;

public interface HomeMVP {

    interface ViewMVP{
        void onSuccesListOffert(ArrayList<GetListOffertResult> getListOffertResults);
        void onSuccesListCategories(ArrayList<GetListCategoriesData> getListCategoriesData);
        void onSuccesSavedProduct(PostSavedProductResponse postSavedProductResponse);
        void onError(String error);
    }
    interface PresenterMVP{
        void onSuccesListOffert(ArrayList<GetListOffertResult> getListOffertResults);
        void onSuccesListCategories(ArrayList<GetListCategoriesData> getListCategoriesData);
        void onSuccesSavedProduct(PostSavedProductResponse postSavedProductResponse);
        void onError(String error);
    }
}
