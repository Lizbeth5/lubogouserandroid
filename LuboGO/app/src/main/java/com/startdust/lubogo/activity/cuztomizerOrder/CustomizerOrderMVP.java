package com.startdust.lubogo.activity.cuztomizerOrder;

import com.startdust.lubogo.retrofit.model.GetInformationProductEstablishmentResponse;
import com.startdust.lubogo.retrofit.model.GetInformationProductEstablishmentResult;
import com.startdust.lubogo.retrofit.model.GetListDetailsProductsResult;

import java.util.ArrayList;

public interface CustomizerOrderMVP {

    interface ViewMVP{
        void onSuccessInformationProduct(GetInformationProductEstablishmentResult getInformationProductEstablishmentResult);
        void onSuccesListDetailsProduct(ArrayList<GetListDetailsProductsResult> getListDetailsProductsResults);
        void onError(String error);
    }

    interface PresenterMVP{
        void onSuccessInformationProduct(GetInformationProductEstablishmentResult getInformationProductEstablishmentResult);
        void onSuccesListDetailsProduct(ArrayList<GetListDetailsProductsResult> getListDetailsProductsResults);
        void onError(String error);
    }
}
