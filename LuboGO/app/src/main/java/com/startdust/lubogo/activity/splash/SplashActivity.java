package com.startdust.lubogo.activity.splash;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.home.MainActivity;
import com.startdust.lubogo.activity.login.LoginActivity;
import com.startdust.lubogo.preferences.AppPreferences;

public class SplashActivity extends AppCompatActivity {
    Button btnStartLogin;
    AppPreferences prefs;
    //private final int DURACION_SPLASH = 3000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        btnStartLogin = findViewById(R.id.btnStart);

        prefs = new AppPreferences(this);

        btnStartLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyLogin();

            }
        });
    }

    private void verifyLogin(){
        if(prefs.isLogued()){
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
            Log.i("status",prefs.isLogued() + "");
        }else{
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    }
}
