package com.startdust.lubogo.activity.verification;

import android.app.Activity;

public class VerificationInteractor {

    Activity activity;
    VerificationMVP.PresenterMVP presenterMVP;

    public VerificationInteractor(Activity activity, VerificationMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;
    }
}
