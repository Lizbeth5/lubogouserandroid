package com.startdust.lubogo.activity.addMethodPayment;

import android.app.Activity;

import com.startdust.lubogo.retrofit.RetrofitModule;

public class AddPaymentMethodInteractor {
    Activity activity;
    AddPaymentMethodMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;

    public AddPaymentMethodInteractor(Activity activity, AddPaymentMethodMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }
}
