package com.startdust.lubogo.retrofit.model;

public class GetDataResponse {
    Boolean response;
    String message;
    String errors;
    GetDataResult result;

    public GetDataResponse() {
    }

    public GetDataResponse(Boolean response, String message, String errors, GetDataResult result) {
        this.response = response;
        this.message = message;
        this.errors = errors;
        this.result = result;
    }

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public GetDataResult getResult() {
        return result;
    }

    public void setResult(GetDataResult result) {
        this.result = result;
    }
}
