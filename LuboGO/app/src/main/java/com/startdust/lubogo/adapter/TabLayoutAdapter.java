package com.startdust.lubogo.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.startdust.lubogo.fragment.menu.MenuTabFragment;
import com.startdust.lubogo.fragment.PizzasTabFragment;
import com.startdust.lubogo.fragment.TickedsTabFragment;

public class TabLayoutAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public TabLayoutAdapter(Context context, FragmentManager fragmentManager, int totalTabs) {
        super(fragmentManager);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                MenuTabFragment menuTabFragment = new MenuTabFragment();
                return menuTabFragment;
            case 1:
                TickedsTabFragment tickedsTabFragment = new TickedsTabFragment();
                return tickedsTabFragment;
            case 2:
                PizzasTabFragment pizzasTabFragment = new PizzasTabFragment();
                return pizzasTabFragment;
            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}
