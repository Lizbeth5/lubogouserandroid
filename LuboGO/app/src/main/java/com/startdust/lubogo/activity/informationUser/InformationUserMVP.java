package com.startdust.lubogo.activity.informationUser;

import com.startdust.lubogo.retrofit.model.GetInformationUserResult;

public interface InformationUserMVP {

    interface ViewMVP{
        void onSuccessInformationUser(GetInformationUserResult getInformationUserResult);
        void onSuccessUpdateInformation();
        void onError(String error);
    }

    interface PresenterMVP{
        void onSuccessInformationUser(GetInformationUserResult getInformationUserResult);
        void onSuccessUpdateInformation();
        void onError(String error);
    }
}
