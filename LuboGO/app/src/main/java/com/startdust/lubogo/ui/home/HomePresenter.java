package com.startdust.lubogo.ui.home;

import android.app.Activity;

import com.startdust.lubogo.retrofit.model.GetListCategoriesData;
import com.startdust.lubogo.retrofit.model.GetListOffertResult;
import com.startdust.lubogo.retrofit.model.PostSavedProductResponse;

import java.util.ArrayList;

public class HomePresenter implements HomeMVP.PresenterMVP{
    Activity activity;
    HomeMVP.ViewMVP viewMVP;
    HomeInteractor homeInteractor;

    public HomePresenter(Activity activity, HomeMVP.ViewMVP viewMVP) {
        this.activity = activity;
        this.viewMVP = viewMVP;

        homeInteractor = new HomeInteractor(activity,this);
    }

    @Override
    public void onSuccesListOffert(ArrayList<GetListOffertResult> getListOffertResults) {
        viewMVP.onSuccesListOffert(getListOffertResults);
    }

    @Override
    public void onSuccesListCategories(ArrayList<GetListCategoriesData> getListCategoriesData) {
        viewMVP.onSuccesListCategories(getListCategoriesData);
    }

    @Override
    public void onSuccesSavedProduct(PostSavedProductResponse postSavedProductResponse) {
        viewMVP.onSuccesSavedProduct(postSavedProductResponse);
    }

    @Override
    public void onError(String error) {
        viewMVP.onError(error);
    }

    public void listOfferts(){
        homeInteractor.listOfferts();
    }

    public void listCategories(){
        homeInteractor.listCategories();
    }

    public void savedProduct(String idUser, String idProduct, String idStatusSaved){
        homeInteractor.savedProduct(idUser, idProduct, idStatusSaved);
    }
}
