package com.startdust.lubogo.activity.email;

import android.app.Activity;

import com.startdust.lubogo.retrofit.RetrofitModule;

public class EditEmailInteractor {
    Activity activity;
    EditEmailMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;

    public EditEmailInteractor(Activity activity, EditEmailMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }
}
