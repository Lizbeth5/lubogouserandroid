package com.startdust.lubogo.activity.category;

import android.app.Activity;
import android.util.Log;

import com.startdust.lubogo.retrofit.RetrofitModule;
import com.startdust.lubogo.retrofit.model.GetListEstablismentResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryInteractor {
     Activity activity;
     CategoryMVP.PresenterMVP presenterMVP;
     RetrofitModule retrofitModule;
     Call<GetListEstablismentResponse> getListEstablismentResponseCall;

    public CategoryInteractor(Activity activity, CategoryMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }

    public void listEstablishment(String id){
        getListEstablismentResponseCall = retrofitModule.getListEstablishment().getListEstablishment(id);
        getListEstablismentResponseCall.enqueue(new Callback<GetListEstablismentResponse>() {
            @Override
            public void onResponse(Call<GetListEstablismentResponse> call, Response<GetListEstablismentResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getResponse()){
                        presenterMVP.onSuccessListEstablishment(response.body().getResult().getData());
                       // Log.i("establecimientos", String.valueOf(response.body().getResult().getData()));
                        Log.i("establecimientos", response.body().getResult().getData().get(0).getEmail());
                    }else{
                        presenterMVP.onError("Error la mostrar información");
                    }
                }

            }

            @Override
            public void onFailure(Call<GetListEstablismentResponse> call, Throwable t) {
                Log.i("error listar", t.getMessage());
            }
        });
    }
}
