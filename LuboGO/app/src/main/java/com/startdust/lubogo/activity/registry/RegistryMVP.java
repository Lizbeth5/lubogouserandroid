package com.startdust.lubogo.activity.registry;

public interface RegistryMVP {

    interface ViewMVP{
        void onSuccessRegistry();
        void onError(String error);
    }

    interface PresenterMVP{
        void onSuccessRegistry();
        void onError(String error);
    }
}
