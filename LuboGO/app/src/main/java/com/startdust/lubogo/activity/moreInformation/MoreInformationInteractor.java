package com.startdust.lubogo.activity.moreInformation;

import android.app.Activity;

import com.startdust.lubogo.retrofit.RetrofitModule;
import com.startdust.lubogo.retrofit.model.GetInformationProfileEstablishmentResponse;
import com.startdust.lubogo.retrofit.model.GetListScheduleServiceEstablishmentResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoreInformationInteractor {
    Activity activity;
    MoreInformationMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;
    Call<GetInformationProfileEstablishmentResponse> getInformationProfileEstablishmentResponseCall;
    Call<GetListScheduleServiceEstablishmentResponse> getListScheduleServiceEstablishmentResponseCall;

    public MoreInformationInteractor(Activity activity, MoreInformationMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }

    public void informationEstablishment(String id){
        getInformationProfileEstablishmentResponseCall = retrofitModule.getInformationProfileEstablishment().getInformationProfileEstablishment(id);
        getInformationProfileEstablishmentResponseCall.enqueue(new Callback<GetInformationProfileEstablishmentResponse>() {
            @Override
            public void onResponse(Call<GetInformationProfileEstablishmentResponse> call, Response<GetInformationProfileEstablishmentResponse> response) {
                if(response.isSuccessful()){
                    presenterMVP.onSuccessInformationProfile(response.body().getResult());
                }else{
                    presenterMVP.onError("Error al mostrar información");
                }
            }

            @Override
            public void onFailure(Call<GetInformationProfileEstablishmentResponse> call, Throwable t) {

            }
        });

    }

    public void listScheduleServiceEstablishment(String id){
        getListScheduleServiceEstablishmentResponseCall = retrofitModule.getListScheduleServiceEstablishment().getListScheduleServiceEstablishment(id);
        getListScheduleServiceEstablishmentResponseCall.enqueue(new Callback<GetListScheduleServiceEstablishmentResponse>() {
            @Override
            public void onResponse(Call<GetListScheduleServiceEstablishmentResponse> call, Response<GetListScheduleServiceEstablishmentResponse> response) {
                if(response.isSuccessful()){
                    presenterMVP.onSuccessListScheduleServiceEstablishment(response.body().getResult().getData());
                }else{
                    presenterMVP.onError("Error al mostrar horario");
                }
            }

            @Override
            public void onFailure(Call<GetListScheduleServiceEstablishmentResponse> call, Throwable t) {

            }
        });
    }
}
