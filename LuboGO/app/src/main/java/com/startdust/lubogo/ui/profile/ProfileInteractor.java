package com.startdust.lubogo.ui.profile;

import android.app.Activity;
import android.util.Log;

import com.startdust.lubogo.preferences.AppPreferences;
import com.startdust.lubogo.retrofit.RetrofitModule;
import com.startdust.lubogo.retrofit.model.GetInformationUserResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileInteractor {
    Activity activity;
    ProfileMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;
    Call<GetInformationUserResponse> getInformationUserResponseCall;
    AppPreferences prefs;

    public ProfileInteractor(Activity activity, ProfileMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
        prefs = new AppPreferences(activity);
    }

    public void informationUser(){
        Log.i("idUsuario", prefs.getUserId());
        getInformationUserResponseCall = retrofitModule.getInformationUser().getInformationUser(prefs.getUserId());
        getInformationUserResponseCall.enqueue(new Callback<GetInformationUserResponse>() {
            @Override
            public void onResponse(Call<GetInformationUserResponse> call, Response<GetInformationUserResponse> response) {
                if(response.isSuccessful()){
                    presenterMVP.onSuccessProfile(response.body().getResult());
                }else{
                    presenterMVP.onError("Error al mostrar información");
                }
            }

            @Override
            public void onFailure(Call<GetInformationUserResponse> call, Throwable t) {
                Log.i("nombre error", t.getMessage());
            }
        });

    }

}
