package com.startdust.lubogo.activity.editNumerPhone;

import android.app.Activity;

public class EditNumberPhonePresenter implements EditNumberPhoneMVP.PresenterMVP {
    Activity activity;
    EditNumberPhoneMVP.ViewMVP viewMVP;
    EditNumberPhoneInteractor interactor;

    public EditNumberPhonePresenter(Activity activity, EditNumberPhoneMVP.ViewMVP viewMVP) {
        this.activity = activity;
        this.viewMVP = viewMVP;

        interactor = new EditNumberPhoneInteractor(activity,this);
    }
}
