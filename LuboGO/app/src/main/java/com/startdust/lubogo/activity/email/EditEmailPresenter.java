package com.startdust.lubogo.activity.email;

import android.app.Activity;

public class EditEmailPresenter implements EditEmailMVP.PresenterMVP {
    Activity activity;
    EditEmailMVP.ViewMVP viewMVP;
    EditEmailInteractor editEmailInteractor;

    public EditEmailPresenter(Activity activity, EditEmailMVP.ViewMVP viewMVP) {
        this.activity = activity;
        this.viewMVP = viewMVP;

        editEmailInteractor = new EditEmailInteractor(activity,this);
    }
}
