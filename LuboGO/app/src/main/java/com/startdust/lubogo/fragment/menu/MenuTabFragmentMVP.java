package com.startdust.lubogo.fragment.menu;

import com.startdust.lubogo.retrofit.model.GetListProductEstablishmentResult;

import java.util.ArrayList;

public interface MenuTabFragmentMVP {
    interface ViewMVP{
        void onSuccessListProducts(ArrayList<GetListProductEstablishmentResult> getListProductEstablishmentResults);
        void onError(String error);
    }

    interface PresenterMVP{
        void onSuccessListProducts(ArrayList<GetListProductEstablishmentResult> getListProductEstablishmentResults);
        void onError(String error);
    }
}
