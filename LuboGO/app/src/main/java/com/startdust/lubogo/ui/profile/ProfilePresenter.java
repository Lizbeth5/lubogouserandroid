package com.startdust.lubogo.ui.profile;

import android.app.Activity;

import com.startdust.lubogo.retrofit.model.GetInformationUserResult;

public class ProfilePresenter implements ProfileMVP.PresenterMVP{
    Activity activity;
    ProfileMVP.ViewMVP viewMVP;
    ProfileInteractor profileInteractor;

    public ProfilePresenter(Activity activity, ProfileMVP.ViewMVP viewMVP) {
        this.activity = activity;
        this.viewMVP = viewMVP;

        profileInteractor = new ProfileInteractor(activity, this);
    }

    @Override
    public void onSuccessProfile(GetInformationUserResult getInformationUserResult) {
        viewMVP.onSuccessProfile(getInformationUserResult);
    }

    @Override
    public void onError(String error) {
        viewMVP.onError(error);
    }

    public void informationUser(){
        profileInteractor.informationUser();

    }

}
