package com.startdust.lubogo.activity.moreInformation;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.startdust.lubogo.R;
import com.startdust.lubogo.adapter.ScheduleServiceEstablishmentAdapter;
import com.startdust.lubogo.retrofit.model.GetInformationProfileEstablishmentResult;
import com.startdust.lubogo.retrofit.model.GetListScheduleServiceEstablishmentData;

import java.util.ArrayList;

public class MoreInformationActivity extends AppCompatActivity implements MoreInformationMVP.ViewMVP{
    ImageView ivCloseMoreInformation, ivImageEstablishment;
    TextView tvNameEstablishment, tvTypeEstablishment, tvQualificationEstablishment, tvQuantityOrders, tvDescriptionEstablishment;
    RecyclerView rvListScheduleServiceEstablishmengt;

    String idEstablecimiento;
    MoreInformationPresenter presenter;
    ScheduleServiceEstablishmentAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_information);

        ivCloseMoreInformation = findViewById(R.id.ivCloseMoreInformation);
        tvNameEstablishment = findViewById(R.id.tvNameEstablishmentInformation);
        ivImageEstablishment = findViewById(R.id.ivImageEstablishmentInformation);
        tvTypeEstablishment = findViewById(R.id.tvTypeEstablishmentInformation);
        tvQualificationEstablishment = findViewById(R.id.tvQualificationEstablishmentInformation);
        tvQuantityOrders = findViewById(R.id.tvQuantityOrders);
        tvDescriptionEstablishment = findViewById(R.id.tvDescriptionEstablishment);
        rvListScheduleServiceEstablishmengt = findViewById(R.id.rvListScheduleServiceEstablishmengt);

        progressDialog = new ProgressDialog(this);

        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL,false);
        presenter = new MoreInformationPresenter(this,this);

        idEstablecimiento = getIntent().getExtras().getString("idEstablecimiento");

        ivCloseMoreInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        presenter.informationEstablishment(idEstablecimiento);
        presenter.listScheduleServiceStablishment(idEstablecimiento);
    }

    @Override
    public void onSuccessInformationProfile(GetInformationProfileEstablishmentResult getInformationProfileEstablishmentResult) {
        progressDialog.setTitle("Cargando");
        progressDialog.setMessage("Cargando información");
        progressDialog.setCancelable(false);
        progressDialog.show();


        Log.i("nombre", getInformationProfileEstablishmentResult.getNombre());
        tvNameEstablishment.setText(getInformationProfileEstablishmentResult.getNombre());
        tvTypeEstablishment.setText("Restaurante");//getInformationProfileEstablishmentResult.getIdTipoEstablecimiento());
        tvQualificationEstablishment.setText(getInformationProfileEstablishmentResult.getCalificacion());
        tvDescriptionEstablishment.setText(getInformationProfileEstablishmentResult.getInformacionEstablecimiento());

        String downloadImage = getInformationProfileEstablishmentResult.getUrlFoto();
        Picasso.with(this)
                .load(downloadImage)
                .into(ivImageEstablishment);

        progressDialog.dismiss();
    }

    @Override
    public void onSuccessListScheduleServiceEstablishment(ArrayList<GetListScheduleServiceEstablishmentData> getListScheduleServiceEstablishmentData) {
        adapter = new ScheduleServiceEstablishmentAdapter(getApplicationContext(),getListScheduleServiceEstablishmentData);
        rvListScheduleServiceEstablishmengt.setLayoutManager(linearLayoutManager);
        rvListScheduleServiceEstablishmengt.setAdapter(adapter);
    }

    @Override
    public void onError(String error) {

    }
}
