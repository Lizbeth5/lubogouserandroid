package com.startdust.lubogo.activity.registry;

import android.app.Activity;
import android.util.Log;

import com.startdust.lubogo.model.User;
import com.startdust.lubogo.retrofit.RetrofitModule;
import com.startdust.lubogo.retrofit.model.SetRegistryResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistryInteractor {
    //logica
    Activity activity;
    RegistryMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;
    Call<SetRegistryResponse> setRegistryResponseCall;

    public RegistryInteractor(Activity activity, RegistryMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }

    //consumir servicio
    public void setRegistry(final User user){
        setRegistryResponseCall = retrofitModule.setRegistry().setRegistry(user.getNombre(), user.getApellidoMaterno(), user.getApellidoPaterno(), user.getEmail(), user.getPassword(), "1");
        setRegistryResponseCall.enqueue(new Callback<SetRegistryResponse>() {
            @Override
            public void onResponse(Call<SetRegistryResponse> call, Response<SetRegistryResponse> response) {
                //Si responde correctamente el servicio
                SetRegistryResponse setRegistryResponse = response.body();
                if(setRegistryResponse.getResponse()){
                    presenterMVP.onSuccessRegistry(); //se consumio el servicio
                }else{
                    presenterMVP.onError("Error al registrar, intente de nuevo.");
                }
            }

            @Override
            public void onFailure(Call<SetRegistryResponse> call, Throwable t) {
                Log.i("Error Registro", t.getMessage());
            }
        });
    }

}
