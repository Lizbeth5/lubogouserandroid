package com.startdust.lubogo.activity.profileEstablishment;

import com.startdust.lubogo.retrofit.model.GetInformationProfileEstablishmentResult;
import com.startdust.lubogo.retrofit.model.GetListProductEstablishmentResult;

import java.util.ArrayList;

public interface ProfileEstablishmentMVP {

    interface ViewMVP{
        void onSuccessInformationEstablishment(GetInformationProfileEstablishmentResult getInformationProfileEstablishmentResult);
        void onSuccessListProductsEstablishment(ArrayList<GetListProductEstablishmentResult> getListProductEstablishmentResults);
        void onError(String error);
    }

    interface PresenterMVP{
        void onSuccessInformationEstablishment(GetInformationProfileEstablishmentResult getInformationProfileEstablishmentResult);
        void onSuccessListProductsEstablishment(ArrayList<GetListProductEstablishmentResult> getListProductEstablishmentResults);
        void onError(String error);
    }
}
