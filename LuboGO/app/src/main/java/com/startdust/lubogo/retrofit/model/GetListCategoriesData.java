package com.startdust.lubogo.retrofit.model;

public class GetListCategoriesData {
    String idTipoEstablecimiento;
    String descripcion;
    String urlImage;

    public GetListCategoriesData() {
    }

    public GetListCategoriesData(String idTipoEstablecimiento, String descripcion, String urlImage) {
        this.idTipoEstablecimiento = idTipoEstablecimiento;
        this.descripcion = descripcion;
        this.urlImage = urlImage;
    }

    public String getIdTipoEstablecimiento() {
        return idTipoEstablecimiento;
    }

    public void setIdTipoEstablecimiento(String idTipoEstablecimiento) {
        this.idTipoEstablecimiento = idTipoEstablecimiento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
