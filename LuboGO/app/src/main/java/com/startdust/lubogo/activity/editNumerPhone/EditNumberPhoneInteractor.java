package com.startdust.lubogo.activity.editNumerPhone;

import android.app.Activity;

import com.startdust.lubogo.retrofit.RetrofitModule;

public class EditNumberPhoneInteractor {
    Activity activity;
    EditNumberPhoneMVP.PresenterMVP presenterMVP;
    RetrofitModule retrofitModule;

    public EditNumberPhoneInteractor(Activity activity, EditNumberPhoneMVP.PresenterMVP presenterMVP) {
        this.activity = activity;
        this.presenterMVP = presenterMVP;

        retrofitModule = new RetrofitModule();
    }
}
