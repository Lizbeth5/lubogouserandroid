package com.startdust.lubogo.activity.login;

public interface LoginMVP {

    interface ViewMVP{
        void onSuccessLogin();
        void onError(String error);
    }

    interface PresenterMVP{
        void onSuccesssLogin();
        void onError(String error);
    }
}
