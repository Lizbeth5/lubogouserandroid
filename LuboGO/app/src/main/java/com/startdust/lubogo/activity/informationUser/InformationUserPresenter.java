package com.startdust.lubogo.activity.informationUser;

import android.app.Activity;

import com.startdust.lubogo.retrofit.model.GetInformationUserResult;

public class InformationUserPresenter implements InformationUserMVP.PresenterMVP {
    Activity activity;
    InformationUserMVP.ViewMVP informationUserMVP;
    InformationUserInteractor informationUserInteractor;

    public InformationUserPresenter(Activity activity, InformationUserMVP.ViewMVP informationUserMVP) {
        this.activity = activity;
        this.informationUserMVP = informationUserMVP;

        informationUserInteractor = new InformationUserInteractor(activity,this);
    }

    @Override
    public void onSuccessInformationUser(GetInformationUserResult getInformationUserResult) {
       informationUserMVP.onSuccessInformationUser(getInformationUserResult);
    }

    @Override
    public void onSuccessUpdateInformation() {
        informationUserMVP.onSuccessUpdateInformation();
    }


    @Override
    public void onError(String error) {
        informationUserMVP.onError(error);
    }

    public void informationUser(){
        informationUserInteractor.informationUser();
    }

    public void updateInformation(String name, String lastName){
        informationUserInteractor.updateInformation(name, lastName);
    }

}
