package com.startdust.lubogo.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.startdust.lubogo.R;
import com.startdust.lubogo.preferences.AppPreferences;
import com.startdust.lubogo.activity.profileEstablishment.ProfileEstablishmentActivity;
import com.startdust.lubogo.retrofit.RetrofitModule;
import com.startdust.lubogo.retrofit.model.GetListOffertResult;
import com.startdust.lubogo.retrofit.model.PostSavedProductResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffertsAdapter extends RecyclerView.Adapter<OffertsAdapter.ViewHolder> {
    Context context;
    ArrayList<GetListOffertResult> data;

    public OffertsAdapter(Context context, ArrayList<GetListOffertResult> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public OffertsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_ofertas, parent, false);
        return new OffertsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.Bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView ivPhotoEstablishentOffers, ivSaveOffers;
        TextView tvNameEstablishmentOffers, tvNameProductOffers, tvQualificationEstablishmentOffers;
        CardView cvOffert;
        RetrofitModule retrofitModule;
        Call<PostSavedProductResponse> postSavedProductResponseCall;
        AppPreferences prefs;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPhotoEstablishentOffers = itemView.findViewById(R.id.ivPhotoEstablishentOffers);
            tvNameEstablishmentOffers = itemView.findViewById(R.id.tvNameEstablishmentOffers);
            tvNameProductOffers = itemView.findViewById(R.id.tvNameProductOffers);
            tvQualificationEstablishmentOffers = itemView.findViewById(R.id.tvQualificationEstablishmentOffers);
            ivSaveOffers = itemView.findViewById(R.id.ivSaveOffers);
            cvOffert = itemView.findViewById(R.id.cvOffert);

            retrofitModule = new RetrofitModule();
            prefs = new AppPreferences(context);
        }

        public void Bind(final GetListOffertResult getListOffertResult){
            Log.i("info", getListOffertResult.getDescripcion());
            tvNameProductOffers.setText(getListOffertResult.getDescripcion());
            tvQualificationEstablishmentOffers.setText(getListOffertResult.getCalificacion());
            tvNameEstablishmentOffers.setText(getListOffertResult.getNombre());

            String downloadPhoto = getListOffertResult.getUrlFoto();
            Picasso.with(context)
                    .load(downloadPhoto)
                    .into(ivPhotoEstablishentOffers);

            ivSaveOffers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                /*    Intent intent = new Intent(context, HomeFragment.class);
                    intent.putExtra("IdProducto", getListOffertResult.getIdProducto());
                    context.startActivity(intent);*/

                    savedProduct(getListOffertResult.getIdProducto(), "1");
                    Log.i("IdProducto", getListOffertResult.getIdProducto());
                    ivSaveOffers.setImageResource(R.drawable.ic_saved_white);
                    ivSaveOffers.setMaxWidth(35);
                    ivSaveOffers.setMaxHeight(35);
                }
            });

            cvOffert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProfileEstablishmentActivity.class);
                    intent.putExtra("idEstablecimiento", getListOffertResult.getIdEstablecimiento());
                    context.startActivity(intent);
                }
            });
        }
        public void savedProduct(String idProduct, String idStatusSaved){
            postSavedProductResponseCall = retrofitModule.postSavedProduct().postSavedProduct(prefs.getUserId(), idProduct, idStatusSaved);
            postSavedProductResponseCall.enqueue(new Callback<PostSavedProductResponse>() {
                @Override
                public void onResponse(Call<PostSavedProductResponse> call, Response<PostSavedProductResponse> response) {
                    if (response.isSuccessful()){
                        if (response.body().getResponse()){
                            Toast.makeText(context, "Producto guardado", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                            //Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                        Toast.makeText(context, "Guardado", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PostSavedProductResponse> call, Throwable t) {
                    Log.i("ErrorListar", t.getMessage());
                }
            });
        }

    }
}
