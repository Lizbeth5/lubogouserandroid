package com.startdust.lubogo.activity.addMethodPayment;

import android.app.Activity;

public class AddPaymentMethodPresenter implements AddPaymentMethodMVP.PresenterMVP {
    Activity activity;
    AddPaymentMethodMVP.ViewMVP viewMVP;
    AddPaymentMethodInteractor addPaymentMethodInteractor;

    public AddPaymentMethodPresenter(Activity activity, AddPaymentMethodMVP.ViewMVP viewMVP) {
        this.activity = activity;
        this.viewMVP = viewMVP;

        addPaymentMethodInteractor = new AddPaymentMethodInteractor(activity,this);
    }
}
