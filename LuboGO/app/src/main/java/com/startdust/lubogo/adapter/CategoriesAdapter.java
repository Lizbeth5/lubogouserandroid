package com.startdust.lubogo.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.startdust.lubogo.R;
import com.startdust.lubogo.activity.category.CategoryActivity;
import com.startdust.lubogo.retrofit.model.GetListCategoriesData;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {
    Context context;
    ArrayList<GetListCategoriesData> data;

    public CategoriesAdapter(Context context, ArrayList<GetListCategoriesData> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_category,parent,false);
        return new CategoriesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.Bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView ivPhotoCategory;
        TextView tvNameCategory;
        RelativeLayout rlCategories;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPhotoCategory = itemView.findViewById(R.id.ivPhotoCategory);
            tvNameCategory = itemView.findViewById(R.id.tvNameCategory);
            rlCategories = itemView.findViewById(R.id.rlCategories);

        }

        public void Bind(final GetListCategoriesData getListCategoriesData){
            tvNameCategory.setText(getListCategoriesData.getDescripcion());

            Log.i("imagen", getListCategoriesData.getUrlImage());
                String downloadImage = getListCategoriesData.getUrlImage();
                Picasso.with(context)
                        .load(downloadImage)
                        .into(ivPhotoCategory);

            rlCategories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CategoryActivity.class);
                    intent.putExtra("idCategoria", getListCategoriesData.getIdTipoEstablecimiento());
                    context.startActivity(intent);
                }
            });
        }
    }
}
