package com.startdust.lubogo.activity.termsAndConditions;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.startdust.lubogo.R;

public class TermsAndConditionsActivity extends AppCompatActivity {
    ImageView ivReturnLegal;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);

        ivReturnLegal = findViewById(R.id.ivReturnLegal);

        ivReturnLegal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
