package com.startdust.lubogo.activity.recoverPassword;

import android.app.Activity;

public class RecoverPasswordPresenter implements  RecoverPasswordMVP.PresenterMVP {
    Activity activity;
    RecoverPasswordMVP.ViewMVP viewMVP;
    RecoverPasswordInteractor recoverPasswordInteractor;

    public RecoverPasswordPresenter(Activity activity, RecoverPasswordMVP.ViewMVP viewMVP) {
        this.activity = activity;
        this.viewMVP = viewMVP;

        recoverPasswordInteractor = new RecoverPasswordInteractor(activity,this);
    }
}
